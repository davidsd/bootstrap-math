bootstrap-math
---------------

A math library for bootstrap computations.

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/bootstrap-math/)
