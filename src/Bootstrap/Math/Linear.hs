module Bootstrap.Math.Linear
  ( module Exports
  , V
  , Matrix
  ) where

import           Bootstrap.Math.Linear.BilinearProduct as Exports
import           Bootstrap.Math.Linear.Instances       as Exports ()
import           Bootstrap.Math.Linear.Literal         as Exports 
import           Bootstrap.Math.Linear.Util            as Exports
import           Data.Matrix.Static                    (Matrix)
import           Linear.V                              (V)
