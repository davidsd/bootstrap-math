{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

-- | A 'FractionalSeries' is a sum of products of a fractional power
-- of x, times a 'Series' in (integer powers of) x, for example:
--
--   x^(1/3)*(1 + 2*x + 3*x^2 + ...)
--
-- Note that this is *not* in general a series in a fractional power
-- of x.

module Bootstrap.Math.FractionalSeries
  ( FractionalSeries
  , singleton
  , toList
  , composeSeries
  , mapSeries
  , truncate
  , eval
  , termList
  , sequenceMatrix
  , sequenceMatrixStatic
  ) where

import           Bootstrap.Math.MonoidRing  (Power (..), type (^))
import           Bootstrap.Math.Series      (Series, pow1p, ($$))
import qualified Bootstrap.Math.Series      as Series
import           Bootstrap.Math.VectorSpace (VectorSpace)
import           Control.Arrow              (first)
import           Control.DeepSeq            (NFData)
import           Data.Binary                (Binary)
import           Data.Map.Strict            (Map)
import qualified Data.Map.Strict            as Map
import           Data.Matrix                (Matrix)
import qualified Data.Matrix.Static         as MatrixStatic
import           Data.Proxy                 (Proxy (..))
import           GHC.TypeLits               (KnownSymbol, symbolVal)
import           GHC.TypeNats               (KnownNat)
import           Prelude                    hiding (truncate)

newtype FractionalSeries s a = MkFractionalSeries (Map (s^Rational) (Series a))
  deriving (Functor)
  deriving newtype (Binary, NFData)

instance (Show a, KnownSymbol s) => Show (FractionalSeries s a) where
  showsPrec p (MkFractionalSeries m) = showParen (p > add_prec) $
    go $ Map.toList m
    where
      go []     = id
      go [t]    = showsTerm t
      go (t:ts) = showsTerm t . showString " + " . go ts
      showsTerm (power, ser) =
        showsPrec (mul_prec+1) power . showString "*" . Series.showsPrecSeries var (mul_prec+1) ser
      add_prec = 6
      mul_prec = 7
      var = symbolVal @s Proxy

singleton :: Num a => s^Rational -> Series a -> FractionalSeries s a
singleton p s = fromList $ [(p,s)]

fromList :: Num a => [(s^Rational, Series a)] -> FractionalSeries s a
fromList xs =
  MkFractionalSeries $
  Map.fromListWith (+)
  [ (Pow (fractionalPart k), Series.shift (floor k) s) | (Pow k, s) <- xs ]
  where
    fractionalPart k = k - fromIntegral (floor k :: Integer)

toList :: FractionalSeries s a -> [(s^Rational, Series a)]
toList (MkFractionalSeries m) = Map.toList m

instance Num a => Num (FractionalSeries s a) where
  MkFractionalSeries m1 + MkFractionalSeries m2 =
    MkFractionalSeries $ Map.unionWith (+) m1 m2
  m1 * m2 =
    fromList $ do
    (p1, s1) <- toList m1
    (p2, s2) <- toList m2
    pure (p1 <> p2, s1 * s2)
  negate (MkFractionalSeries m) = MkFractionalSeries $ Map.map negate m
  abs    (MkFractionalSeries m) = MkFractionalSeries $ Map.map abs    m
  signum (MkFractionalSeries m) = MkFractionalSeries $ Map.map signum m
  fromInteger 0 = MkFractionalSeries Map.empty
  fromInteger n = MkFractionalSeries $ Map.singleton 0 (fromInteger n)

instance VectorSpace (FractionalSeries s)

composeSeries
  :: (Eq a, Fractional a)
  => FractionalSeries s a
  -> Series a
  -> FractionalSeries s' a
composeSeries fser ser' =
  fromList $ fmap composeTerm $ toList fser
  where
    composeTerm (Pow p, ser) =
      (Pow p
      , (fmap fromRational (pow1p p) $$ Series.shift (-1) ser' - 1) *
        (ser `Series.composeLaurent` ser')
      )

mapSeries :: (Series a -> Series b) -> FractionalSeries s a -> FractionalSeries s b
mapSeries f (MkFractionalSeries m) = MkFractionalSeries $ Map.map f m

truncate :: Num a => Rational -> FractionalSeries s a -> FractionalSeries s a
truncate ord f = fromList $ do
  (Pow p, ser) <- toList f
  pure $ (Pow p, Series.truncate (ceiling (ord - p)) ser)

-- | Evaluate a 'FractionalSeries'.
--
-- WARNING: An attempt to evaluate an infinite 'FractionalSeries' will
-- result in an exception. You should truncate the series first.
eval :: Floating a => a -> FractionalSeries s a -> a
eval a f = sum $ do
  (Pow p, ser) <- toList f
  pure $ a ** fromRational p * Series.eval a ser

intersperse :: Num a => [(Rational, a)] -> [(Rational, a)] -> [(Rational, a)]
intersperse xs [] = xs
intersperse [] xs = xs
intersperse (x@(r,a):xs) (y@(s,b):ys) = case compare r s of
  EQ -> (r,a+b) : intersperse xs ys
  LT -> x : intersperse xs (y:ys)
  GT -> y : intersperse (x:xs) ys

-- | A list of terms in decreasing order of the power of 's'. Note:
-- for an infinite series, this returns an infinite list.
termList :: Num a => FractionalSeries s a -> [(s^Rational, a)]
termList (MkFractionalSeries m)
  | Map.null m = []
  | otherwise = fmap (first Pow) $ foldl1 intersperse $ do
      (Pow r, s) <- Map.toList m
      pure $ fmap (first (\n -> r + fromIntegral n)) (Series.toList s)

-- | Turn a 'Matrix' of 'FractionalSeries' into a 'FractionalSeries'
-- of 'Matrix's. This is easier to implement with MatrixStatic because
-- it has a well-defined zero. Note: the name "sequence" comes from
-- 'Data.Traversable.sequenceA'.
sequenceMatrixStatic
  :: forall m n a s . (Num a, KnownNat m, KnownNat n)
  => MatrixStatic.Matrix m n (FractionalSeries s a)
  -> FractionalSeries s (MatrixStatic.Matrix m n a)
sequenceMatrixStatic mat = sum $
  MatrixStatic.matrix @m @n $ \ij ->
  fmap (singleElem ij) $ mat MatrixStatic.! ij
  where
    singleElem ij a = MatrixStatic.unsafeSet a ij 0

-- | Version of 'sequenceMatrixStatic' with non-static Matrices
sequenceMatrix
  :: Num a
  => Matrix (FractionalSeries s a)
  -> FractionalSeries s (Matrix a)
sequenceMatrix mat = MatrixStatic.withStatic mat $
  fmap MatrixStatic.unpackStatic .
  sequenceMatrixStatic
