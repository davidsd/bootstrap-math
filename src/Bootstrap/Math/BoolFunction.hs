{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFunctor  #-}
{-# LANGUAGE DeriveGeneric  #-}

module Bootstrap.Math.BoolFunction
  ( BoolFunction
  , mkBoolFunction
  , initial
  , final
  , switchPoints
  , not
  , constant
  , step
  , interval
  , trueIntervals
  , TwoPtCompact(..)
  , toHalfCircle
  , and
  , or
  , eval
  , All(..)
  , Any(..)
  ) where

import           Data.Binary           (Binary)
import           Data.List             (group, sort)
import           Data.Set              (Set)
import qualified Data.Set              as Set
import           GHC.Generics          (Generic)
import qualified Linear.Metric         as L
import           Linear.V              (V)
import           Prelude               hiding (and, not, or)
import qualified Prelude               as P
import qualified Bootstrap.Math.Linear      as L
import           Bootstrap.Math.VectorSpace ((^/))

-- | A 'BoolFunction a' is formally a function from 'TwoPtCompact a'
-- to 'Maybe Bool', where 'Nothing' signifies that the BoolFunction is
-- discontinuous there. We represent it by its value at 'NegInfinity',
-- together with a 'Set' of finite points where it switches value.
data BoolFunction a = MkBoolFunction Bool (Set a)
  deriving (Show, Eq, Ord, Generic, Binary)

-- | The two-point compactification of 'a', obtained by adding
-- NegInfinity and PosInfinity.
data TwoPtCompact a = NegInfinity | PosInfinity | Finite a
  deriving (Eq, Show)

instance Ord a => Ord (TwoPtCompact a) where
  compare NegInfinity NegInfinity = EQ
  compare PosInfinity PosInfinity = EQ
  compare NegInfinity _           = LT
  compare _ PosInfinity           = LT
  compare PosInfinity _           = GT
  compare _ NegInfinity           = GT
  compare (Finite x) (Finite y)   = compare x y

instance Bounded (TwoPtCompact a) where
  minBound = NegInfinity
  maxBound = PosInfinity

mkBoolFunction :: Ord a => Bool -> [a] -> BoolFunction a
mkBoolFunction b ys = MkBoolFunction b (nubSwitches ys)
  where
    nubSwitches xs = foldMap squish (group xs)
      where
        squish (x:xs') | length xs' `mod` 2 == 0 = Set.singleton x
        squish _ = Set.empty

-- | The value of a 'BoolFunction' at 'NegInfinity'
initial :: BoolFunction a -> Bool
initial (MkBoolFunction b _) = b

-- | The value of a 'BoolFunction' at 'PosInfinity'
final :: BoolFunction a -> Bool
final (MkBoolFunction b xs) | length xs `mod` 2 == 0 = b
final (MkBoolFunction b _)  = P.not b

-- | Locations at which a 'BoolFunction' switches value
switchPoints :: BoolFunction a -> [a]
switchPoints (MkBoolFunction _ xs) = Set.toList xs

-- | Point-wise not
not :: BoolFunction a -> BoolFunction a
not (MkBoolFunction b xs) = MkBoolFunction (P.not b) xs

-- | A constant 'BoolFunction'.
constant :: Ord a => Bool -> BoolFunction a
constant b = mkBoolFunction b []

-- | A step function that is False on (-infinity,x) and True on
-- [x,infinity)
step :: Ord a => a -> BoolFunction a
step x = mkBoolFunction False [x]

-- | A 'BoolFunction' that is True in the interval between l and u
interval :: Ord a => a -> a -> BoolFunction a
interval l u = step l `and` not (step u)

-- | Turn a 'BoolFunction' into a Set of switches. The switch (x,b)
-- represents a switch to the value b. For example,
--
-- > derivative (step x) == [(x,True)]
--
derivative :: BoolFunction a -> [(a, Bool)]
derivative (MkBoolFunction b xSet) = zip xs (cycle [P.not b, b])
  where
    xs = Set.toList xSet

-- | Turn a list of switches into an BoolFunction, accumulating
-- multiple true/false switches. We should have:
--
-- > integral (fromEnum $ initial b) (derivative b) == b
--
-- 'switches' is assumed to be sorted.
integral :: Ord a => Int -> [(a, Bool)] -> BoolFunction a
integral start switches = go start switches []
  where
    go _ [] acc             = mkBoolFunction (start > 0) (reverse acc)
    go 1 ((x,False):xs) acc = go 0             xs (x:acc)
    go n ((_,False):xs) acc = go (max 0 (n-1)) xs acc
    go 0 ((x,True) :xs) acc = go 1             xs (x:acc)
    go n ((_,True) :xs) acc = go (n+1)         xs acc

-- | Evaluate a 'BoolFunction' at a point
eval :: Ord a => a -> BoolFunction a -> Maybe Bool
eval y b@(MkBoolFunction _ switches)
  | Set.member y switches = Nothing
  | otherwise = Just $ final $
                integral (fromEnum $ initial b) $ filter (\(x,_) -> x <= y) (derivative b)

-- | Pointwise 'or'
or :: Ord a => BoolFunction a -> BoolFunction a -> BoolFunction a
or i1 i2 =
  integral
  (fromEnum (initial i1) + fromEnum (initial i2))
  (sort (derivative i1 <> derivative i2))

-- | Pointwise 'and'
and :: Ord a => BoolFunction a -> BoolFunction a -> BoolFunction a
and i1 i2 = not $ not i1 `or` not i2

-- | Map a 'TwoPtCompact' to a vector on the unit circle with
-- nonnegative first component. +-infinity map to unit vectors
-- pointing up/down.
toHalfCircle :: Floating a => TwoPtCompact a -> V 2 a
toHalfCircle (Finite x) = v ^/ L.norm v
  where
    v = L.V2 1 x
toHalfCircle PosInfinity = L.V2 0 1
toHalfCircle NegInfinity = L.V2 0 (-1)

-- | A sorted list of non-overlapping intervals where the BoolFunction is
-- positive. For example,
--
-- > trueIntervals (interval l u) = [(Finite l, Finite u)] -- if u > l
-- > trueIntervals (interval l u) = []                     -- if u <= l
-- > trueIntervals (step x)       = [(Finite x, PosInfinity)]
--
trueIntervals :: BoolFunction a -> [(TwoPtCompact a, TwoPtCompact a)]
trueIntervals (MkBoolFunction b xSet) = case (b, Set.toList xSet) of
  (False, xs)    -> go xs
  (True, x : xs) -> (NegInfinity, Finite x) : go xs
  (True, [])     -> [(NegInfinity, PosInfinity)]
  where
    go []           = []
    go [x]          = [(Finite x, PosInfinity)]
    go (x : y : xs) = (Finite x, Finite y) : go xs

-- | A Monoid for combining BoolFunctions with 'and'
newtype All a = MkAll { getAll :: BoolFunction a }

instance Ord a => Semigroup (All a) where
  MkAll b1 <> MkAll b2 = MkAll $ b1 `and` b2

instance Ord a => Monoid (All a) where
  mempty = MkAll $ constant True

-- | A Monoid for combining BoolFunctions with 'or'
newtype Any a = MkAny { getAny :: BoolFunction a }

instance Ord a => Semigroup (Any a) where
  MkAny b1 <> MkAny b2 = MkAny $ b1 `or` b2

instance Ord a => Monoid (Any a) where
  mempty = MkAny $ constant False
