{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

-- | A 'Series' is a power series with arbitrary integer leading power
-- that can be infinte or finite in length. A finite 'Series'
-- represents the mathematical expression
--
-- a*x^n + b*x^(n+1) + ... + O(x^m)
--
-- where O(x^m) with m>=n is an unknown term. Arithmetic with 'Series'
-- will compute as many terms as possible from the known data.
--
-- A 'Series' is a wrapper around a 'Stream' of coefficients, and
-- inherits many of its mathematical operations from operations on
-- 'Stream's.
module Bootstrap.Math.Series
  ( SeriesEnd(..)
  , Series(..)
  , showsPrecSeries
  , dropLeadingZeros
  , fromStream
  , fromPolynomial
  , leadingExponent
  , leadingTerm
  , seriesEnd
  , toList
  , mapIndexed
  , x
  , o
  , constant
  , mulWith
  , divWith
  , compose
  , composeLaurent
  , ($$)
  , ($^)
  , shift
  , collapse
  , truncate
  , eval
  , truncatePolynomial
  , truncateEval
  , truncateTest
  , fitRationalPol
  , applyTaylor
  , pow
  , pow1p
  , exp0
  , log1p
  , sin0
  , cos0
  , asin0
  ) where

import Control.DeepSeq (NFData(..))
import qualified Data.Vector as V
import           Bootstrap.Math.Polynomial  (Polynomial)
import qualified Bootstrap.Math.Polynomial  as Polynomial
import           Bootstrap.Math.Stream      (Stream (..))
import qualified Bootstrap.Math.Stream      as Stream
import           Bootstrap.Math.VectorSpace (VectorSpace)
import qualified Bootstrap.Math.VectorSpace as VS
import           Data.Binary                (Binary (..))
import           Prelude                    hiding (map, truncate)

-- | A power series with arbitrary integer leading power that can be
-- infinite or finite in length.
--
-- MkSeries n (FiniteSeries m) (a:|b:|...) represents
--
--   a*x^n + b*x^(n-1) + ... + O(x^m)
--
-- MkSeries n InfiniteSeries (a:|b:|...) represents
--
--   a*x^n + b*x^(n-1) + ...
--
-- Invariants:
--   - The start of the series is less than or equal to SeriesEnd
--   - The behavior of the series should be independent of the stream
--     elements at SeriesEnd or beyond
--
-- Keep in mind: the first few elements of a 'Series' might
-- vanish. Such leading zeros are dropped automatically in division
-- (in the denominator) or series composition (in the argument). If
-- you need to ensure such terms are absent, use 'dropLeadingZeros'.
data Series a = MkSeries Int SeriesEnd (Stream a)
  deriving (Functor)

-- | For convenience, we provide a 'Binary' instance for 'Series'.
--
-- WARNING: This instance is *partial*. It only works for finite
-- series. An attempt to serialize an infinite Series will result in
-- an Exception.
instance Binary a => Binary (Series a) where
  put (MkSeries _ InfiniteSeries _) = error "Cannot serialize an infinite series"
  put (MkSeries n (FiniteSeries n') s) = do
    put n
    put n'
    put $ Stream.take (n' - n) s
  get = do
    n <- get
    n' <- get
    coeffs <- get
    pure $ MkSeries n (FiniteSeries n') $
      Stream.prefix coeffs $
      Stream.repeat $ error "Undefined Series coefficients"

-- | Again, we define a *partial* instance for NFData. This function
-- will loop if called on an infinite series.
instance NFData a => NFData (Series a) where
  rnf (MkSeries _ InfiniteSeries _) = error "Cannot rnf an infinite series"
  rnf (MkSeries n (FiniteSeries n') s) =
    rnf n `seq`
    rnf n' `seq`
    go n s
    where
      go m (a:|as)
        | m == n' = ()
        | otherwise = rnf a `seq` go (m+1) as

-- | The end of a series -- i.e. the location of the first unknown
-- term. 'FiniteSeries n' represents O(x^n), while 'InfiniteSeries'
-- indicates that all terms are (in principle) known.
data SeriesEnd = InfiniteSeries | FiniteSeries Int
  deriving (Eq)

instance Ord SeriesEnd where
  compare InfiniteSeries   (FiniteSeries _) = GT
  compare (FiniteSeries _) InfiniteSeries   = LT
  compare InfiniteSeries   InfiniteSeries   = EQ
  compare (FiniteSeries m) (FiniteSeries n) = compare m n

-- | Map a function over end location, if it is finite.
mapEnd :: (Int -> Int) -> SeriesEnd -> SeriesEnd
mapEnd _ InfiniteSeries   = InfiniteSeries
mapEnd f (FiniteSeries n) = FiniteSeries (f n)

-- | Apply a function to every coefficient in a series, where the
-- function can depend on the power of the given term.
mapIndexed :: (Int -> a -> b) -> Series a -> Series b
mapIndexed f (MkSeries n n' s) =
  MkSeries n n' (Stream.zipWith f (fmap (n+) Stream.nonnegativeIntegers) s)

-- | The leading exponent of x at which the series is nonzero, or
-- unknown (whichever is first).
leadingExponent :: (Eq a, Num a) => Series a -> Int
leadingExponent = fst . leadingTerm

leadingTerm :: (Eq a, Num a) => Series a -> (Int, a)
leadingTerm s = case dropLeadingZeros s of
  MkSeries n _ (a :| _) -> (n, a)

-- | Drop leading terms with zero coefficients. This is needed for
-- normalizing 'Series' before certain mathematica operations like
-- division and composition. Note: this function will loop if applied
-- to the zero series.
dropLeadingZeros :: (Eq a, Num a) => Series a -> Series a
dropLeadingZeros s@(MkSeries k (FiniteSeries l) _) | k >= l = s
dropLeadingZeros (MkSeries n n' (0 :| s)) = dropLeadingZeros (MkSeries (n+1) n' s)
dropLeadingZeros s = s

-- | Convert a 'Stream' of coefficients to a 'Series' starting at x^0.
fromStream :: Stream a -> Series a
fromStream = MkSeries 0 InfiniteSeries

-- | Convert a Stream of coefficients to a series, where the 'Stream'
-- is guaranteed not to identically vanish (it can still have zero
-- elements), and drop the leading zeros from the Series.
fromNonzeroStream :: (Eq a, Num a) => Stream a -> Series a
fromNonzeroStream = dropLeadingZeros . fromStream

-- | The end of the Series
seriesEnd :: Series a -> SeriesEnd
seriesEnd (MkSeries _ n' _) = n'

-- | Show the 'Series' using 'var' as the variable.
showsPrecSeries :: Show a => String -> Int -> Series a -> ShowS
showsPrecSeries var p s =
  showParen (p > add_prec) $
  case s of
    MkSeries n InfiniteSeries (a:|_) ->
      showsTerm a n . showString " + ..."
    MkSeries n (FiniteSeries m) (a:|as)
      | n == m    -> showString "O(" . showString var . showString "^" . showsPrec (exp_prec+1) n . showString ")"
      | otherwise -> showsTerm a n . showString " + " . showsPrecSeries var add_prec (MkSeries (n+1) (FiniteSeries m) as)
  where
    add_prec = 6
    mul_prec = 7
    exp_prec = 8
    showsTerm a n = showsPrec (mul_prec+1) a . showString "*" . showString var . showString "^" . showsPrec (exp_prec+1) n

instance Show a => Show (Series a) where
  showsPrec = showsPrecSeries "x"

instance Num a => Num (Series a) where
  (+) = addSeries
  (*) = mulSeries
  negate = fmap negate
  abs    = fmap abs
  signum = fmap signum
  fromInteger n = MkSeries 0 InfiniteSeries (fromInteger n)

instance (Eq a, Fractional a) => Fractional (Series a) where
  (/) = divSeries
  fromRational r = MkSeries 0 InfiniteSeries (fromRational r)

instance (Eq a, Floating a) => Floating (Series a) where
  pi = MkSeries 0 InfiniteSeries (pi :| Stream.repeat 0)
  exp    = applyTaylor Stream.expExpansion
  log    = applyTaylor Stream.logExpansion
  sin    = applyTaylor Stream.sinExpansion
  cos    = applyTaylor Stream.cosExpansion
  asin   = applyTaylor Stream.asinExpansion
  acos   = applyTaylor Stream.acosExpansion
  atan   = applyTaylor Stream.atanExpansion
  sinh a = VS.scale (1/2) (exp a - exp (-a))
  cosh a = VS.scale (1/2) (exp a + exp (-a))
  asinh  = applyTaylor Stream.asinhExpansion
  acosh  = applyTaylor Stream.acoshExpansion
  atanh  = applyTaylor Stream.atanhExpansion

pow :: (Eq a, Floating a) => a -> Series a -> Series a
pow = applyTaylor . Stream.binomialExpansion

-- | The series expansion of (1+x)^c
pow1p :: (Eq a, Fractional a) => a -> Series a
pow1p = fromNonzeroStream . Stream.binomial1pExpansion

-- | Series expansions of exp x, sin x, cos x, asin x, etc.. These are
-- equivalent to simply 'exp x', 'sin x', etc.. However, these Series
-- expansions around special points only require a 'Fractional'
-- instance, whereas e.g. 'exp x' requires a 'Floating' instance. This
-- can be useful for exact arithmetic.
exp0, log1p, sin0, cos0, asin0 :: (Eq a, Fractional a) => Series a
exp0  = fromNonzeroStream Stream.exp0Expansion
log1p = fromNonzeroStream Stream.log1pExpansion
sin0  = fromNonzeroStream Stream.sin0Expansion
cos0  = fromNonzeroStream Stream.cos0Expansion
asin0 = fromNonzeroStream Stream.asin0Expansion

instance VectorSpace Series where
  type IsBaseField Series a = Num a
  scale a = fmap (a*)

-- | A list of known terms in the Series, together with their powers
-- of 'x'
toList :: Series a -> [(Int, a)]
toList ser = case mapIndexed (,) ser of
  MkSeries _ InfiniteSeries s -> Stream.toList s
  MkSeries n (FiniteSeries k) s -> take (k-n) $ Stream.toList s

-- | A 'Stream' of coefficients starting from the location
-- x^k. Careful: if the given Series has a finite number of known
-- terms, then the coefficients beyond the last known term are
-- ill-defined.
coeffsFrom :: Num a => Int -> Series a -> Stream a
coeffsFrom k (MkSeries l _ s)
  | k > l     = Stream.drop (k-l) s
  | otherwise = Stream.prefix (replicate (l-k) 0) s

addSeries :: Num a => Series a -> Series a -> Series a
addSeries s@(MkSeries n n' _) t@(MkSeries m m' _) =
  MkSeries k (min n' m') (coeffsFrom k s + coeffsFrom k t)
  where
    k = min n m

mulSeries :: Num a => Series a -> Series a -> Series a
mulSeries (MkSeries n n' s) (MkSeries m m' t) =
  MkSeries (n+m) (min (mapEnd (n+) m') (mapEnd (m+) n')) (s*t)

-- | Multiply two series with a general combining operation for
-- coefficients.
mulWith :: Num c => (a -> b -> c) -> Series a -> Series b -> Series c
mulWith mul (MkSeries n n' s) (MkSeries m m' t) =
  MkSeries (n+m) (min (mapEnd (n+) m') (mapEnd (m+) n')) $
  Stream.mulWith mul s t

divSeries :: (Eq a, Fractional a) => Series a -> Series a -> Series a
divSeries (MkSeries n n' s) s2 =
  let MkSeries m m' t = dropLeadingZeros s2
  in MkSeries (n-m) (min (mapEnd (subtract m) n') (mapEnd ((n-2*m)+) m')) (s/t)

-- | Divide two series with general div and mul operations for
-- coefficients.
divWith :: (Num a, Eq b, Num b) => (a -> b -> a) -> (a -> b -> a) -> Series a -> Series b -> Series a
divWith div' mul' (MkSeries n n' s) s2 =
  let MkSeries m m' t = dropLeadingZeros s2
  in MkSeries (n-m) (min (mapEnd (subtract m) n') (mapEnd ((n-2*m)+) m')) $
     Stream.divWith div' mul' s t

-- | Shift the exponents in the series by 'k'. This is equivalent to
-- multiplying by x^k.
shift :: Int -> Series a -> Series a
shift k (MkSeries n n' s) = MkSeries (n+k) (mapEnd (k+) n') s

-- | Make the given series finite, with end no later than
-- x^k. Equivalent to adding 'o k'.
truncate :: Num a => Int -> Series a -> Series a
truncate k s = s + o k

-- | The Series 'x': an infinite series whose only nonzero coefficient
-- is the linear term. This can be used as a building block for more
-- general series. For example:
--
-- >>> 1/(1-x) -- an infinite series
-- 1.0*x^0 + ...
--
-- >>> 1/(1-x) + o 3 -- a finite series ending at x^3
-- 1.0*x^0 + 1.0*x^1 + 1.0*x^2 + O(x^3)
--
-- >>> exp x + o 3 -- a finite series ending at x^4
-- 1.0*x^0 + 1.0*x^1 + 0.5*x^2 + 0.16666*x^3 + O(x^4)
x :: Num a => Series a
x = MkSeries 1 InfiniteSeries 1

-- | 'o k' is the error term O(x^k). 'truncate k s' is equivalent to
-- 's + o k'.
o :: Num a => Int -> Series a
o k = MkSeries k (FiniteSeries k) 0

-- | A series whose only nonzero coefficient is the constant term,
-- given by 'a'.
constant :: Num a => a -> Series a
constant a = MkSeries 0 InfiniteSeries (a :| Stream.repeat 0)

-- | Drop leading zeros until we get to the x^k term. If there are
-- nonzero coefficients before that, return Nothing.
dropZerosToTerm :: (Eq a, Num a) => Int -> Series a -> Maybe (Series a)
dropZerosToTerm k s@(MkSeries n _ _) | n >= k = Just s
dropZerosToTerm k (MkSeries n n' (0:|xs))     = dropZerosToTerm k (MkSeries (n+1) n' xs)
dropZerosToTerm _ _ = Nothing

-- | Drop leading zeros until we get to the x^1 term. If there are
-- nonzero coefficients before that, return Nothing.
dropZerosToLinearTerm :: (Eq a, Num a) => Series a -> Maybe (Series a)
dropZerosToLinearTerm = dropZerosToTerm 1

-- | The composition of two 'Series', as functions.
--
-- WARNING: This function will throw an exception if the first series
-- contains negative exponents. The reason is that we use '(^)' to
-- raise the argument to the leading power, so that this function only
-- requires a 'Num' instance.
--
-- TODO: Handle the constant term -- separately, since it is exact
compose :: (Eq a, Num a) => Series a -> Series a -> Series a
compose (MkSeries n n' s) arg
  | n < 0 = error "The first argument of Series.compose cannot contain\
                  \ negative exponents. Use Series.composeLaurent instead."
  | Just t@(MkSeries m m' _) <- dropZerosToLinearTerm arg =
      let
        n'' = mapEnd (subtract n) n'
        tCoeffs = coeffsFrom 0 t
      in
        t^n * MkSeries 0 (min (mapEnd (subtract m) m') (mapEnd (m*) n'')) (Stream.compose s tCoeffs)
  | otherwise = error "Cannot compose series if the argument has order < 1"

-- | The same as 'compose', except the base field must be
-- 'Fractional', so that we can deal with negative exponents.
composeLaurent :: (Eq a, Fractional a) => Series a -> Series a -> Series a
composeLaurent (MkSeries n n' s) arg
  | Just t@(MkSeries m m' _) <- dropZerosToLinearTerm arg =
      let
        n'' = mapEnd (subtract n) n'
        tCoeffs = coeffsFrom 0 t
      in
        t^^n * MkSeries 0 (min (mapEnd (subtract m) m') (mapEnd (m*) n'')) (Stream.compose s tCoeffs)
  | otherwise = error "Cannot compose series if the argument has order < 1"

-- | An infix synonym for 'compose'. The symbol is supposed to evoke
-- function application, since composing series is like applying a
-- function.
($$) :: (Eq a, Num a) => Series a -> Series a -> Series a
($$) = compose
infixr 0 $$

-- | Compose two series, where the second 'Series' has coefficients in
-- a 'VectorSpace' over the first.
($^) :: (VectorSpace v, Eq (v a), Num (v a), Eq a, Num a, VS.IsBaseField v a) => Series a -> Series (v a) -> Series (v a)
($^) f a = fmap VS.fromScalar f $$ a
infixr 0 $^

-- | Given a function 'taylorAround' that computes the Taylor
-- expansion coefficients of a function around its argument, apply
-- that function to 's'.
applyTaylor :: (Num a, Eq a) => (a -> Stream a) -> Series a -> Series a
applyTaylor taylorAround arg =
  compose (MkSeries 0 InfiniteSeries (taylorAround a)) s'
  where
    s = dropLeadingZeros arg
    (a,s') = case s of
      MkSeries n _ _
        | n < 0 ->
          error "Cannot apply a taylor series to a power series with negative leading exponent"
      MkSeries 0 n' (f:|fs) -> (f, MkSeries 1 n' fs)
      _ -> (0, s)

-- | Collapse a 'Stream' of 'Series' into a single 'Series'. We assume
-- each coefficient 'Series' has only positive powers of 'x': an
-- attempt to use this function with Laurent Series will result in an
-- error!
collapse :: (Eq a, Num a) => Series (Series a) -> Series a
collapse (MkSeries n n' s) = MkSeries n n' $ Stream.collapse $ fmap getCoeffs s
  where
    getCoeffs t = case dropZerosToTerm 0 t of
      Just t' -> coeffsFrom 0 t'
      Nothing -> error "Cannot collapse a Series whose coefficients contain negative powers."

-- | Evaluate a finite 'Series' on the given argument. This will throw
-- an error if called on an Infinite series.
eval :: Fractional a => a -> Series a -> a
eval _ (MkSeries _ InfiniteSeries _) = error "Cannot evaluate an infinite series"
eval a (MkSeries n (FiniteSeries n') s) =
  a^^n * foldr (\c r -> c + a*r) 0 (Stream.take (n'-n) s)

-- | Truncate a Series to a power x^n, times a polynomial of degree at
-- most 'ord-n-1'. The first element of the returned tuple is 'n', and
-- the second is the Polynomial. The series is truncated from
-- x^ord. If some of the terms of the series before x^ord are unknown,
-- the Polynomial is truncated there. For example
--
-- >>> truncatePolynomial 3 (1/(1-x) + o 3)
-- (0, 1+x+x^2) -- Polynomial has degree 2
--
-- >>> truncatePolynomial 4 (1/(1-x) + o 3)
-- (0, 1+x+x^2) -- Polynomial still has degree 2
truncatePolynomial :: (Eq a, Num a) => Int -> Series a -> (Int, Polynomial a)
truncatePolynomial ord (MkSeries n n' s) =
  (n, Polynomial.fromList (Stream.take len s))
  where
    deg = ord - n - 1
    len = case n' of
      InfiniteSeries -> deg+1
      FiniteSeries k -> min (deg+1) (k-n)

-- | Truncate 's' at order x^ord and evaluate at 'a'. This is useful
-- for using the 'Series' as an approxximation to a function. For
-- example,
--
-- >>> truncateEval 4 0.1 $ 1/(1-x)
-- 1.111
truncateEval :: Fractional a => Int -> a -> Series a -> a
truncateEval ord a = eval a . truncate ord

-- | The difference between 'f eps' and the truncated series expansion
-- of 'f' with order 'ord' evaluated at 'eps'. For a sufficiently
-- smooth function, this should get exponentially smaller as 'ord' is
-- increased.
--
-- >>> truncateTest (\x -> log (x+1)) 7 0.1
-- 1.3137658266826335e-8
--
truncateTest :: (Floating a, Eq a) => (forall b . Floating b => b -> b) -> Int -> a -> a
truncateTest f ord eps = f eps - truncateEval ord eps (f x)

-- | Fit a 'Polynomial Rational' of degree 'deg' to the given function
-- at the point x0 (where it assumed to be smooth). We evaluate the
-- function as a 'Series' and then truncate the 'Series'.
--
-- >>> fitRationalPol 3 2 (\x -> 1/(1-x))
-- x^3 - 7*x^2 + 17*x - 15
fitRationalPol
  :: Int
  -> Rational
  -> (forall a . Fractional a => a -> a)
  -> Polynomial Rational
fitRationalPol deg x0 f =
  case truncatePolynomial (deg+1) $ f (x + constant x0) of
    (k,p)
      | k >= 0 ->
          Polynomial.compose (Polynomial.x^k * p) (Polynomial.x - Polynomial.constant x0)
      | otherwise -> error "Expected polynomial, but got a Laurent series"

-- | Convert a Polynomial to a Series
fromPolynomial :: (Eq a, Num a) => Polynomial a -> Series a
fromPolynomial pol = case V.toList (Polynomial.coefficients pol) of
  [] -> 0
  cs -> fromNonzeroStream $ Stream.fromList cs
