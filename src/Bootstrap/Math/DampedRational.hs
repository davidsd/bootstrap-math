{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies       #-}

module Bootstrap.Math.DampedRational where

import Bootstrap.Math.Polynomial       (Polynomial)
import Bootstrap.Math.Polynomial       qualified as Pol
import Bootstrap.Math.RationalFunction qualified as R
import Bootstrap.Math.VectorSpace      (VectorSpace (..))
import Control.DeepSeq                 (NFData)
import Control.Exception               (ArithException (DivideByZero),
                                        Exception, throw)
import Data.Aeson                      (ToJSON (..), object, (.=))
import Data.Binary                     (Binary (..))
import Data.Foldable                   qualified as Foldable
import Data.Functor.Compose            (Compose (..))
import Data.Functor.Identity           (Identity (..))
import Data.List                       (intercalate)
import Data.MultiSet                   qualified as MultiSet
import Data.Proxy                      (Proxy (..))
import Data.Reflection                 (Reifies (..))
import GHC.Generics                    (Generic)
import Linear.V2                       (V2 (..))
import Numeric.Limits                  (epsilon)
import Prelude                         hiding (sequence, sum)
import Prelude qualified

data DampedRational base f a = DampedRational
  { numerator :: f (Polynomial a)
  , poles     :: MultiSet.MultiSet Rational
  } deriving stock (Generic)

instance (Binary a) => Binary (MultiSet.MultiSet a) where
    put s = put $ MultiSet.toAscOccurList s
    get   = fmap MultiSet.fromDistinctAscOccurList get

deriving instance (Binary (f (Polynomial a))) => Binary (DampedRational base f a)
deriving instance (Eq (f (Polynomial a)))     => Eq     (DampedRational base f a)
deriving instance (Ord (f (Polynomial a)))    => Ord    (DampedRational base f a)

instance (NFData a, NFData (f (Polynomial a))) => NFData (DampedRational base f a)

instance ( Show (f (Polynomial a))
         , Reifies base a
         , Show a
         , Fractional a
         , Eq a) => Show (DampedRational base f a) where
  show (DampedRational num ps) =
    (if b == 1 then "" else show b ++ "^x*") ++
    "(" ++ show num ++ ")" ++
    if MultiSet.null ps
    then ""
    else "/(" ++
         intercalate "*" [ "(" ++ show @(Polynomial a) (Pol.x - Pol.constant (fromRational p)) ++ ")"
                           ++ if n == 1 then "" else "^" ++ show n
                         | (p, n) <- MultiSet.toOccurList ps
                         ] ++
         ")"
    where
      b = reflect @base Proxy

instance (ToJSON a, Reifies base a, ToJSON (f (Polynomial a))) => ToJSON (DampedRational base f a) where
  toJSON d = object
    [ "numerator" .= numerator d
    , "poles" .= MultiSet.toList (poles d)
    , "base" .= base d
    ]

instance Applicative v => VectorSpace (DampedRational base v) where
  type IsBaseField (DampedRational base v) a = (Fractional a, Eq a)
  zero = DampedRational (pure 0) MultiSet.empty
  scale = scaleDampedRational
  add = addDampedRational

mapNumerator
  :: (f (Polynomial a) -> g (Polynomial b))
  -> DampedRational base f a
  -> DampedRational base g b
mapNumerator f (DampedRational pol ps) = DampedRational (f pol) ps

scaleDampedRational :: (Functor f, Num a, Eq a) => a -> DampedRational base f a -> DampedRational base f a
scaleDampedRational a = mapNumerator (fmap (scale a))

base :: forall b f a . Reifies b a => DampedRational b f a -> a
base _ = reflect @b Proxy

shift
  :: forall base f a . (Functor f, Floating a, Eq a, Reifies base a)
  => Rational
  -> DampedRational base f a
  -> DampedRational base f a
shift 0 d = d
shift x d@(DampedRational pol ps) = DampedRational
  { numerator = fmap (scale expFactor . Pol.shift (fromRational x)) pol
  , poles = MultiSet.map (subtract x) ps
  }
  where
    expFactor = exp (fromRational x * log (base d))

-- | Evaluate a 'DampedRational' at x. This function is labeled
-- "unsafe" because it does not take care to cancel zeros and poles at
-- the location x before evaluating. Instead of using 'evalUnsafe',
-- consider using 'evalCancelPoleZeros'.
evalUnsafe
  :: forall base f a . (Functor f, Reifies base a, Floating a)
  => a
  -> DampedRational base f a
  -> f a
evalUnsafe x (DampedRational pol ps) =
  fmap (\p -> d * Pol.eval p x) pol
  where
    b = reflect @base Proxy
    d = exp (x * log b) / product [ (x - fromRational p)^n | (p, n) <- MultiSet.toOccurList ps ]

data One a

instance Num a => Reifies (One a) a where
  reflect _ = 1

data EInv a

instance Floating a => Reifies (EInv a) a where
  reflect _ = exp (-1)

fromPol :: forall base f a . f (Polynomial a) -> DampedRational base f a
fromPol p = DampedRational p MultiSet.empty

constant :: (Functor f, Num a, Eq a) => f a -> DampedRational (One a) f a
constant c = fromPol (fmap Pol.constant c)

sequence
  :: (Fractional a, Eq a, Functor f, Foldable f, Functor v)
  => f (DampedRational base v a)
  -> DampedRational base (Compose f v) a
sequence ds = DampedRational
  { numerator = Compose (fmap togetherNumerator ds)
  , poles     = allPoles
  }
  where
    allPoles = Foldable.foldl' MultiSet.maxUnion MultiSet.empty (map poles (Foldable.toList ds))
    togetherNumerator (DampedRational pol ps) = fmap (q*) pol
      where
        q = multiSetToPol (MultiSet.difference allPoles ps)

multiSetToPol :: (Fractional a, Eq a) => MultiSet.MultiSet Rational -> Polynomial a
multiSetToPol ps = product [ (Pol.x - Pol.constant (fromRational p))^n
                           | (p, n) <- MultiSet.toOccurList ps
                           ]

addDampedRational
  :: forall base v a . (Fractional a, Eq a, Applicative v)
  => DampedRational base v a
  -> DampedRational base v a
  -> DampedRational base v a
addDampedRational d1 d2 =
  mapNumerator (\(Compose (V2 p1 p2)) -> liftA2 (+) p1 p2) $
  sequence (V2 d1 d2)

-- | Determine whether the numerator of a DampedRational is almost
-- zero at x. We set the scale of the numerator by dividing by all
-- factors in the denominator that are nonvanishing at x.
numeratorAlmostZeroAt
  :: (RealFloat a, Foldable v, Functor v)
  => Rational
  -> DampedRational base v a
  -> Bool
numeratorAlmostZeroAt x (DampedRational pols ps) =
  Foldable.maximum numSizes < sqrt epsilon
  where
    numSizes = fmap (\p -> abs (Pol.eval p (fromRational x) / fromRational den)) pols
    -- | A product of all factors in the denominator not equal to x
    den = product [ (x - p)^n
                  | (p, n) <- MultiSet.toOccurList ps
                  , p /= x
                  ]

-- | Given dr = P / ((x-p) Q), returns Just (reduced, singularPart),
-- where
--
-- > reduced = (P `quot` (x-p)) / Q
-- > singularPart =  P(p) / Q(x)
--
-- Returns Nothing if the denominator does not contain a factor of
-- (x-p).  For simplicity, we do not write the exponential factor
-- base^x, which comes along for the ride. We should have the property
--
-- dr = reduced + singularPart
reduceRationalFactor
  :: (Fractional a, Eq a, Functor f)
  => Rational
  -> DampedRational base f a
  -> Maybe (DampedRational base f a, DampedRational base f a)
reduceRationalFactor p (DampedRational pol ps)
  | MultiSet.member p ps = Just (reduced, singularPart)
  | otherwise = Nothing
  where
    factor = Pol.x - Pol.constant (fromRational p)
    reduced = DampedRational (fmap (`Pol.quot` factor) pol) (MultiSet.delete p ps)
    singularPart = DampedRational (fmap (Pol.constant . (`Pol.eval` fromRational p)) pol) ps

-- | Cancel poles and zeros between the numerator and denominator at x
-- (if they exist). The presence of a zero is determined by
-- 'numeratorAlmostZeroAt'.
cancelPoleZeros
  :: (RealFloat a, Foldable f, Functor f)
  => Rational
  -> DampedRational base f a
  -> DampedRational base f a
cancelPoleZeros x dr = case reduceRationalFactor x dr of
  Just (dr', singularPart)
    | numeratorAlmostZeroAt x singularPart -> cancelPoleZeros x dr'
    | otherwise -> dr
  Nothing -> dr

-- | Evaluate a DampedRational, making sure to cancel poles and zeros
-- at the evaluation point if they exist. The criterion for a zero is
-- determined by numeratorAlmostZeroAt. Throws a 'DivideByZero'
-- exception if the poles at x are not completely cancelled.
evalCancelPoleZeros
  :: (RealFloat a, Foldable f, Functor f, Reifies base a)
  => Rational
  -> DampedRational base f a
  -> f a
evalCancelPoleZeros x dr =
  if MultiSet.member x (poles drCancelled)
  then throw DivideByZero
  else evalUnsafe (fromRational x) drCancelled
  where
    drCancelled = cancelPoleZeros x dr

-- | Cancel poles and zeros at the location of the rightmost pole (if
-- it exists)
cancelRightmostPoleZeros
  :: (RealFloat a, Foldable f, Functor f)
  => DampedRational base f a
  -> DampedRational base f a
cancelRightmostPoleZeros dr =
  case fmap fst (MultiSet.maxView (poles dr)) of
    Just rightmostPole -> cancelPoleZeros rightmostPole dr
    Nothing            -> dr

data DenominatorDoesntFactorizeException = DenominatorDoesntFactorizeException (R.RationalFunction Rational)
  deriving (Show, Exception)

fromRationalFunction
  :: (Fractional a, Eq a)
  => R.RationalFunction Rational
  -> DampedRational (One a) Identity a
fromRationalFunction fn =
  if Prelude.sum (map snd roots) < Pol.degree (R.denominator fn)
  then throw (DenominatorDoesntFactorizeException fn)
  else DampedRational (Identity . (Pol.mapCoefficients fromRational) . R.numerator $ fn) (MultiSet.fromOccurList roots)
  where
    roots = Pol.rationalRoots (R.denominator fn)

multiplyRationalFunction
  :: (Functor f, Fractional a, Eq a)
  => R.RationalFunction Rational
  -> DampedRational base f a
  -> DampedRational base f a
multiplyRationalFunction r (DampedRational num ps) =
  let DampedRational (Identity num') ps' = fromRationalFunction r
  in DampedRational (fmap (num'*) num) (MultiSet.union ps' ps)

evalDerivative
  :: forall base f a . (RealFloat a, Reifies base a, Functor f)
  => a
  -> DampedRational base f a
  -> f a
evalDerivative x (DampedRational num ps) =
  flip fmap num $ \p ->
  (Pol.eval p x * (log b - logq') + Pol.eval (Pol.derivative p) x)*bx/q
  where
    q     = product [ (x - fromRational p)^n | (p, n) <- MultiSet.toOccurList ps ]
    logq' = Prelude.sum [ fromIntegral n / (x - fromRational p) | (p, n) <- MultiSet.toOccurList ps ]
    b     = reflect @base Proxy
    bx    = exp (x * log b)
