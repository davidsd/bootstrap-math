{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE MultiWayIf         #-}
{-# LANGUAGE PatternSynonyms    #-}
{-# LANGUAGE StaticPointers     #-}
{-# LANGUAGE ViewPatterns       #-}

module Bootstrap.Math.HalfInteger where

import Control.DeepSeq (NFData)
import Data.Aeson      (FromJSON (..), ToJSON (..))
import Data.Binary     (Binary)
import Data.Ratio      (denominator, numerator)
import Data.Scientific (Scientific)
import GHC.Generics    (Generic)
import Hyperion.Static (Dict (..), Static (..), cPtr)
import Prelude         hiding (toInteger)

-- | An integer, or an integer plus 1/2.
newtype HalfInteger = HalfInteger { twice :: Integer}
  deriving (Generic)
  deriving newtype (Eq, Ord, Binary, NFData)

instance Static (Binary HalfInteger) where closureDict = cPtr (static Dict)

instance ToJSON HalfInteger where
  toJSON     = toJSON     . fromHalfInteger @Scientific
  toEncoding = toEncoding . fromHalfInteger @Scientific

instance FromJSON HalfInteger where
  parseJSON = fmap realToFrac . parseJSON @Scientific

instance Num HalfInteger where
  fromInteger i = HalfInteger (2*i)
  negate (HalfInteger twoJ) = HalfInteger (-twoJ)
  abs (HalfInteger twoJ) = HalfInteger (abs twoJ)
  signum (HalfInteger twoJ) = fromInteger (signum twoJ)
  (HalfInteger twoJ1) + (HalfInteger twoJ2) = HalfInteger (twoJ1 + twoJ2)
  -- | Warning: 'HalfInteger's can't always be multiplied. We handle
  -- the case where one or both of the HalfIntegers is actually an
  -- Integer, but throw an error otherwise.
  (HalfInteger twoJ1) * (HalfInteger twoJ2) =
    case (divMod twoJ1 2, divMod twoJ2 2) of
      ((j1, 0), _) -> HalfInteger (j1*twoJ2)
      (_, (j2, 0)) -> HalfInteger (j2*twoJ1)
      (_, _)       ->
        error "Product of two half-integers is not a half-integer"

-- | Morally, 'HalfInteger' should not be an instance of 'Fractional':
-- they cannot be converted to from an arbitrary Rational, and they do
-- not always have a 'HalfInteger' reciprocal. However, we define this
-- (broken) Fractional instance so that we can use nice syntax for
-- HalfInteger like 0.5, etc.
instance Fractional HalfInteger where
  -- | Throws an error if the denominator is not 1 or 2
  fromRational r = case (numerator r, denominator r) of
    (n, 1) -> fromInteger n
    (n, 2) -> HalfInteger n
    _      -> error "Rational number is not a half-integer"
  -- | Throws an error if the denominator is not 1 or 2
  (HalfInteger twoJ1) / (HalfInteger twoJ2) =
    fromRational (fromIntegral twoJ1 / fromIntegral twoJ2)

instance Show HalfInteger where
  show i | i < 0     = '-':showPositive (-i)
         | otherwise = showPositive i
    where
      showPositive (HalfInteger twoJ) = case divMod twoJ 2 of
        (n, 0) -> show n
        (n, 1) -> show n ++ ".5"
        _      -> error "Impossible result for divMod by 2"

instance Real HalfInteger where
  toRational = fromHalfInteger

instance RealFrac HalfInteger where
  properFraction (HalfInteger i) = (fromIntegral q, HalfInteger r)
    where
      (q,r) = i `quotRem` 2

instance Enum HalfInteger where
  succ x   = x + 1
  pred x   = x - 1
  toEnum n = fromIntegral n
  fromEnum = fromInteger . truncate
  enumFrom = iterate succ
  enumFromThen h@(HalfInteger n) (HalfInteger m) =
    iterate (HalfInteger . (+ step) . twice) h
    where step = m - n
  enumFromTo h h' = takeWhile (<= h') $ enumFrom h
  enumFromThenTo h@(HalfInteger n) (HalfInteger m) h' =
    takeWhile (if step > 0 then (<= h') else (>= h')) $
    iterate (HalfInteger . (+ step) . twice) h
    where step = m - n

fromHalfInteger :: Fractional a => HalfInteger -> a
fromHalfInteger (HalfInteger x) = fromIntegral x / 2

toInteger :: HalfInteger -> Maybe Integer
toInteger (HalfInteger i) = case divMod i 2 of
  (d,0) -> Just d
  _     -> Nothing

toEvenInteger :: HalfInteger -> Maybe Integer
toEvenInteger i = case toInteger i of
  Just i' | even i' -> Just i'
  _                 -> Nothing

toOddInteger :: HalfInteger -> Maybe Integer
toOddInteger i = case toInteger i of
  Just i' | odd i' -> Just i'
  _                -> Nothing

isInteger :: HalfInteger -> Bool
isInteger = maybe False (const True) . toInteger

isEvenInteger :: HalfInteger -> Bool
isEvenInteger = maybe False (const True) . toEvenInteger

isOddInteger :: HalfInteger -> Bool
isOddInteger = maybe False (const True) . toOddInteger

pattern AnInteger :: (Integral a, Num a) => a -> HalfInteger
pattern AnInteger i <- (fmap fromIntegral . toInteger -> Just i) where
  AnInteger i = HalfInteger (2 * fromIntegral i)

pattern EvenInteger :: (Integral a, Num a) => a -> HalfInteger
pattern EvenInteger i <- (fmap fromIntegral . toEvenInteger -> Just i)

pattern OddInteger :: (Integral a, Num a) => a -> HalfInteger
pattern OddInteger i <- (fmap fromIntegral . toOddInteger -> Just i)
