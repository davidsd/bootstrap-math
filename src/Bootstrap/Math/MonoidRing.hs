{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE DerivingVia                #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

-- | A 'MonoidRing m a' is the set of formal sums \sum_g r_g g, where
-- 'g' runs over elements of the 'Monoid' 'm', and r_g are
-- coefficients in 'a'. As a module over 'a', it is equivalent to
-- 'FreeVect m a'. However, multiplication uses the underlying '<>' of
-- m.

module Bootstrap.Math.MonoidRing where

import           Bootstrap.Math.FreeVect    (FreeVect)
import qualified Bootstrap.Math.FreeVect    as FreeVect
import           Bootstrap.Math.Polynomial  (Polynomial)
import qualified Bootstrap.Math.Polynomial  as Polynomial
import           Bootstrap.Math.VectorSpace (VectorSpace (..), fromScalar, (*^))
import           Control.DeepSeq            (NFData)
import           Data.Aeson                 (FromJSON, FromJSONKey, ToJSON)
import           Data.Binary                (Binary)
import           Data.Map.Strict            (Map)
import           Data.Monoid                (Sum (..))
import           Data.Proxy                 (Proxy (..))
import           GHC.Generics               (Generic)
import           GHC.TypeLits               (KnownSymbol, symbolVal)
import           Prelude                    hiding (sum)

newtype MonoidRing m a = MkMonoidRing { getMonoidRing :: FreeVect m a }
  deriving newtype (Eq, Ord, Show, Binary, FromJSON, ToJSON, NFData)

instance (Monoid m, Ord m) => VectorSpace (MonoidRing m) where
  type IsBaseField (MonoidRing m) a = (Num a, Eq a)
  scale x (MkMonoidRing y) = MkMonoidRing $ scale x y
  sum = MkMonoidRing . sum . fmap getMonoidRing

instance (Num a, Eq a, Monoid m, Ord m) => Num (MonoidRing m a) where
  MkMonoidRing p + MkMonoidRing q = MkMonoidRing $ p + q
  MkMonoidRing p * MkMonoidRing q = MkMonoidRing $ FreeVect.multiplyWith (<>) p q
  negate (MkMonoidRing p) = MkMonoidRing $ negate p
  abs    (MkMonoidRing p) = MkMonoidRing $ abs p
  signum (MkMonoidRing p) = MkMonoidRing $ signum p
  fromInteger n = MkMonoidRing $ fromInteger n *^ FreeVect.vec mempty

-- | A monomial in the 'MonoidRing'.
monomial :: (Num a, Eq a) => m -> MonoidRing m a
monomial = MkMonoidRing . FreeVect.vec

-- | Combine two 'MonoidRing's with a general combining
-- operation. '(*)' is equivalent to 'multiplyWith <>'.
multiplyWith
  :: (Num a, Eq a, Ord l)
  => (m -> n -> l)
  -> MonoidRing m a
  -> MonoidRing n a
  -> MonoidRing l a
multiplyWith f (MkMonoidRing x) (MkMonoidRing y) =
  MkMonoidRing $ FreeVect.multiplyWith f x y

-- | Apply the given function to each basis element and sum the
-- results as elements of the base field.
applyFunctional :: Num a => (m -> a) -> MonoidRing m a -> a
applyFunctional f = FreeVect.applyFunctional f . getMonoidRing

-- | Apply the given function to each basis element and sum the
-- results as the elements of a vector space.
evalBasis :: (VectorSpace v, IsBaseField v k) => (b -> v k) -> MonoidRing b k -> v k
evalBasis f = FreeVect.evalFreeVect f . getMonoidRing

-- | An infix version of 'flip evalBasis'
(/.) :: (VectorSpace v, IsBaseField v k) => MonoidRing b k -> (b -> v k) -> v k
(/.) = flip evalBasis
infixl 1 /.

fromList :: (Num a, Eq a, Ord m) => [(m,a)] -> MonoidRing m a
fromList = MkMonoidRing . FreeVect.fromList

singleton :: (Num a, Eq a, Ord m) => m -> a -> MonoidRing m a
singleton e c = fromList [(e,c)]

toList :: MonoidRing m a -> [(m,a)]
toList = FreeVect.toList . getMonoidRing

mapCoeffs :: (Num a2, Eq a2) => (a1 -> a2) -> MonoidRing m a1 -> MonoidRing m a2
mapCoeffs f = MkMonoidRing . FreeVect.mapCoeffs f . getMonoidRing

mapBasis :: (Num k, Eq k, Ord b2) => (b1 -> b2) -> MonoidRing b1 k -> MonoidRing b2 k
mapBasis f = MkMonoidRing . FreeVect.mapBasis f . getMonoidRing

bindTerms :: (VectorSpace v, IsBaseField v k2) => MonoidRing b1 k1 -> (b1 -> k1 -> v k2) -> v k2
bindTerms (MkMonoidRing e) f = FreeVect.bindTerms e f

-- | An infix version of bindTerms
(//) :: (VectorSpace v, IsBaseField v k2) => MonoidRing b1 k1 -> (b1 -> k1 -> v k2) -> v k2
(//) = bindTerms
infixl 1 //

-- | Group terms according to the action of 'f' on their basis
-- elements.
groupTermsBy :: (Ord b', Ord b) => (b -> b') -> MonoidRing b a -> Map b' (MonoidRing b a)
groupTermsBy f (MkMonoidRing m) = fmap MkMonoidRing $ FreeVect.groupTermsBy f m

-- | A power of the variable x. (Pow y :: Power x a) represents x^y,
-- and has the appropriate Monoid instance. Note that products of
-- powers of multiple variables can be obtained using tuples: (Power
-- "x" Int, Power "y" Int), since tuples inherit a Monoid instance
-- from their elements. Using the notation below, we can write this as
-- ("x"^Int, "y"^Int).
newtype Power x a = Pow a
  deriving newtype (Eq, Ord, Binary, Num, Fractional, NFData)
  deriving stock (Functor, Generic)
  deriving (Semigroup, Monoid) via Sum a
  deriving anyclass (ToJSON, FromJSON, FromJSONKey)

instance (KnownSymbol s, Show a) => Show (Power s a) where
  showsPrec _ (Pow n) = showString (symbolVal @s Proxy) . showString "^" . showsPrec 8 n

-- | Suggestive notation
type (^) = Power

-- | MonoidRing (s^Int) a is essentially a sparse Laurent series in s
-- with coefficients of type a.
type SparseLaurent s a = MonoidRing (s^Int) a

evalPow :: RealFloat a => a -> s^Rational -> a
evalPow x (Pow a) = x ** fromRational a

-- | Convert a sparse Laurent series in s to a leading power of s, together
-- with a Polynomial
toPolynomial :: (Eq a, Num a) => SparseLaurent s a -> (s^Int, Polynomial a)
toPolynomial m = case toList m of
  [] -> (0, 0)
  (pow@(Pow k), c) : xs ->
    (pow, Polynomial.fromList (c : intersperseZeros k xs))
  where
    intersperseZeros _ [] = []
    intersperseZeros k ((Pow k', c) : xs)
      | k >= k'   = error "intersperseZeros applied to un-sorted list"
      | otherwise = replicate (k' - k - 1) 0 ++ [c] ++ intersperseZeros k' xs

-- | Convert a Polynomial to a sparse Laurent series
fromPolynomial :: (Eq a, Num a) => Polynomial a -> SparseLaurent s a
fromPolynomial p = Polynomial.eval (Polynomial.mapCoefficients fromScalar p) (monomial 1)

testFromToPolynomial :: (Eq a, Num a) => Polynomial a -> Bool
testFromToPolynomial p =
  let (Pow q, p') = toPolynomial (fromPolynomial p)
  in q >= 0 && p' * Polynomial.x ^ q == p

deriv :: (Eq a, Num a) => SparseLaurent s a -> SparseLaurent s a
deriv m = fromList $ do
  (Pow k, c) <- toList m
  pure (Pow (k-1), fromIntegral k * c)

