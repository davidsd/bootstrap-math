module Bootstrap.Math.Util where

import           Data.Vector                       (Vector)
import qualified Data.Vector                       as V
import qualified Math.Combinatorics.Exact.Binomial as Binomial
import           Prelude                    hiding ((^), (^^))
import qualified Prelude

factorial :: Int -> Integer
factorial = (factorials !!)
  where
    factorials = scanl (*) 1 [1..]

pochhammer :: (Integral i, Fractional a) => a -> i -> a
pochhammer x k
  | k < 0     = 1 / pochhammer (x + fromIntegral k) (-k)
  | otherwise = product [ x + fromIntegral j | j <- [0 .. k-1] ]

-- | We specialize 'choose' to 'Integer's to avoid overflow
choose :: Integral a => a -> a -> Integer
choose a b = Binomial.choose (toInteger a) (toInteger b)

dot :: Num a => Vector a -> Vector a -> a
dot u v = V.foldl' (+) 0 $ V.zipWith (*) u v

-- | Exponentiation that is less polymorphic in the second argument
-- than the definitions in the Prelude.
infixr 8 ^, ^^
(^) :: Num a => a -> Int -> a
(^) = (Prelude.^)

(^^) :: Fractional a => a -> Int -> a
(^^) = (Prelude.^^)
