{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE PatternSynonyms        #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE ViewPatterns           #-}

module Bootstrap.Math.Linear.Literal
  ( pattern V1
  , pattern V2
  , pattern V3
  , pattern V4
  , pattern V5
  , pattern V6
  , pattern M11
  , pattern M22
  , toV
  , fromV
  , toM
  , fromM
  ) where

import           Data.Kind          (Type)
import           Data.Matrix.Static (Matrix)
import qualified Data.Matrix.Static as M
import           Data.Vector        ((!))
import qualified Data.Vector        as V
import           GHC.TypeNats       (KnownNat)
import           Linear.V           (V (..))

pattern V1 :: a -> V 1 a
pattern V1 a <- (fromV -> a) where
  V1 a = toV a
{-# COMPLETE V1 #-}

pattern V2 :: a -> a -> V 2 a
pattern V2 a1 a2 <- (fromV -> (a1,a2)) where
  V2 a1 a2 = toV (a1,a2)
{-# COMPLETE V2 #-}

pattern V3 :: a -> a -> a -> V 3 a
pattern V3 a1 a2 a3 <- (fromV -> (a1,a2,a3)) where
  V3 a1 a2 a3 = toV (a1,a2,a3)
{-# COMPLETE V3 #-}

pattern V4 :: a -> a -> a -> a -> V 4 a
pattern V4 a1 a2 a3 a4 <- (fromV -> (a1,a2,a3,a4)) where
  V4 a1 a2 a3 a4 = toV (a1,a2,a3,a4)
{-# COMPLETE V4 #-}

pattern V5 :: a -> a -> a -> a -> a -> V 5 a
pattern V5 a1 a2 a3 a4 a5 <- (fromV -> (a1,a2,a3,a4,a5)) where
  V5 a1 a2 a3 a4 a5 = toV (a1,a2,a3,a4,a5)
{-# COMPLETE V5 #-}

pattern V6 :: a -> a -> a -> a -> a -> a -> V 6 a
pattern V6 a1 a2 a3 a4 a5 a6 <- (fromV -> (a1,a2,a3,a4,a5,a6)) where
  V6 a1 a2 a3 a4 a5 a6 = toV (a1,a2,a3,a4,a5,a6)
{-# COMPLETE V6 #-}

pattern M11 :: a -> Matrix 1 1 a
pattern M11 a <- (fromM -> a) where
  M11 a = toM a
{-# COMPLETE M11 #-}

pattern M22 :: a -> a -> a -> a -> Matrix 2 2 a
pattern M22 a1 a2 a3 a4 <- (fromM -> ((a1,a2),(a3,a4))) where
  M22 a1 a2 a3 a4 = toM ((a1,a2),(a3,a4))
{-# COMPLETE M22 #-}

class KnownNat n => TupleVector n where
  type HomogeneousTuple n a :: Type
  toV :: HomogeneousTuple n a -> V n a
  fromV :: V n a -> HomogeneousTuple n a

instance TupleVector 0 where
  type HomogeneousTuple 0 a = ()
  toV _ = V V.empty
  fromV _ = ()

{-
-- | The instances below were created using this function.
tupleVectorInstance :: Int -> String
tupleVectorInstance n = concat
  [ "instance TupleVector ", show n, " where\n"
  , "  type HomogeneousTuple ", show n, " a = (", intercalate "," (replicate n "a"), ")\n"
  , "  toV (", vs "", ") =\n"
  , "    V (V.fromListN ", show n, " [", vs "", "])\n"
  , "  fromV (V v) = (", vs "!", ")\n"
  ]
  where
    vs sep = intercalate "," ["v"++sep++show i | i <- [0 .. n-1]]
-}

instance TupleVector 1 where
  type HomogeneousTuple 1 a = (a)
  toV (v0) =
    V (V.fromListN 1 [v0])
  fromV (V v) = (v!0)

instance TupleVector 2 where
  type HomogeneousTuple 2 a = (a,a)
  toV (v0,v1) =
    V (V.fromListN 2 [v0,v1])
  fromV (V v) = (v!0,v!1)

instance TupleVector 3 where
  type HomogeneousTuple 3 a = (a,a,a)
  toV (v0,v1,v2) =
    V (V.fromListN 3 [v0,v1,v2])
  fromV (V v) = (v!0,v!1,v!2)

instance TupleVector 4 where
  type HomogeneousTuple 4 a = (a,a,a,a)
  toV (v0,v1,v2,v3) =
    V (V.fromListN 4 [v0,v1,v2,v3])
  fromV (V v) = (v!0,v!1,v!2,v!3)

instance TupleVector 5 where
  type HomogeneousTuple 5 a = (a,a,a,a,a)
  toV (v0,v1,v2,v3,v4) =
    V (V.fromListN 5 [v0,v1,v2,v3,v4])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4)

instance TupleVector 6 where
  type HomogeneousTuple 6 a = (a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5) =
    V (V.fromListN 6 [v0,v1,v2,v3,v4,v5])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5)

instance TupleVector 7 where
  type HomogeneousTuple 7 a = (a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6) =
    V (V.fromListN 7 [v0,v1,v2,v3,v4,v5,v6])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6)

instance TupleVector 8 where
  type HomogeneousTuple 8 a = (a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7) =
    V (V.fromListN 8 [v0,v1,v2,v3,v4,v5,v6,v7])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7)

instance TupleVector 9 where
  type HomogeneousTuple 9 a = (a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8) =
    V (V.fromListN 9 [v0,v1,v2,v3,v4,v5,v6,v7,v8])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8)

instance TupleVector 10 where
  type HomogeneousTuple 10 a = (a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9) =
    V (V.fromListN 10 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9)

instance TupleVector 11 where
  type HomogeneousTuple 11 a = (a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10) =
    V (V.fromListN 11 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10)

instance TupleVector 12 where
  type HomogeneousTuple 12 a = (a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11) =
    V (V.fromListN 12 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11)

instance TupleVector 13 where
  type HomogeneousTuple 13 a = (a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12) =
    V (V.fromListN 13 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12)

instance TupleVector 14 where
  type HomogeneousTuple 14 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13) =
    V (V.fromListN 14 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13)

instance TupleVector 15 where
  type HomogeneousTuple 15 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14) =
    V (V.fromListN 15 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14)

instance TupleVector 16 where
  type HomogeneousTuple 16 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15) =
    V (V.fromListN 16 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14,v!15)

instance TupleVector 17 where
  type HomogeneousTuple 17 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16) =
    V (V.fromListN 17 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14,v!15,v!16)

instance TupleVector 18 where
  type HomogeneousTuple 18 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17) =
    V (V.fromListN 18 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14,v!15,v!16,v!17)

instance TupleVector 19 where
  type HomogeneousTuple 19 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18) =
    V (V.fromListN 19 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14,v!15,v!16,v!17,v!18)

instance TupleVector 20 where
  type HomogeneousTuple 20 a = (a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a)
  toV (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19) =
    V (V.fromListN 20 [v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19])
  fromV (V v) = (v!0,v!1,v!2,v!3,v!4,v!5,v!6,v!7,v!8,v!9,v!10,v!11,v!12,v!13,v!14,v!15,v!16,v!17,v!18,v!19)

class (KnownNat m, KnownNat n) => TupleMatrix m n where
  type HomogeneousTupleMatrix m n a :: Type
  toM :: HomogeneousTupleMatrix m n a -> Matrix m n a
  fromM :: Matrix m n a -> HomogeneousTupleMatrix m n a

{-
-- | The instances below were created using this function
tupleMatrixInstance :: Int -> Int -> String
tupleMatrixInstance m n = concat
  [ "instance TupleMatrix ", show m, " ", show n, " where\n"
  , "  type HomogeneousTupleMatrix ", show m, " ", show n, " a = ", mat "(" ")" (\_ _ -> "a"), "\n"
  , "  toM ", mat "(" ")" (\i j -> "a_" ++ show i ++ "_" ++ show j), " =\n"
  , "    M.fromListsUnsafe\n"
  , "    ", mat "[" "]" (\i j -> "a_" ++ show i ++ "_" ++ show j), "\n"
  , "  fromM m =\n"
  , "    ", mat "(" ")" (\i j -> "M.getElem @"++show i++" @"++show j++" m"), "\n"
  ]
  where
    tuple open close k go = open ++ intercalate "," (map go [1 .. k]) ++ close
    mat open close go = tuple open close m $ \i -> tuple open close n (go i)
-}

instance TupleMatrix 1 1 where
  type HomogeneousTupleMatrix 1 1 a = ((a))
  toM ((a_1_1)) =
    M.fromListsUnsafe
    [[a_1_1]]
  fromM m =
    ((M.getElem @1 @1 m))

instance TupleMatrix 1 2 where
  type HomogeneousTupleMatrix 1 2 a = ((a,a))
  toM ((a_1_1,a_1_2)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m))

instance TupleMatrix 1 3 where
  type HomogeneousTupleMatrix 1 3 a = ((a,a,a))
  toM ((a_1_1,a_1_2,a_1_3)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m))

instance TupleMatrix 1 4 where
  type HomogeneousTupleMatrix 1 4 a = ((a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m))

instance TupleMatrix 1 5 where
  type HomogeneousTupleMatrix 1 5 a = ((a,a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4,a_1_5)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4,a_1_5]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m,M.getElem @1 @5 m))

instance TupleMatrix 2 1 where
  type HomogeneousTupleMatrix 2 1 a = ((a),(a))
  toM ((a_1_1),(a_2_1)) =
    M.fromListsUnsafe
    [[a_1_1],[a_2_1]]
  fromM m =
    ((M.getElem @1 @1 m),(M.getElem @2 @1 m))

instance TupleMatrix 2 2 where
  type HomogeneousTupleMatrix 2 2 a = ((a,a),(a,a))
  toM ((a_1_1,a_1_2),(a_2_1,a_2_2)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2],[a_2_1,a_2_2]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m),(M.getElem @2 @1 m,M.getElem @2 @2 m))

instance TupleMatrix 2 3 where
  type HomogeneousTupleMatrix 2 3 a = ((a,a,a),(a,a,a))
  toM ((a_1_1,a_1_2,a_1_3),(a_2_1,a_2_2,a_2_3)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3],[a_2_1,a_2_2,a_2_3]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m))

instance TupleMatrix 2 4 where
  type HomogeneousTupleMatrix 2 4 a = ((a,a,a,a),(a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4),(a_2_1,a_2_2,a_2_3,a_2_4)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4],[a_2_1,a_2_2,a_2_3,a_2_4]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m))

instance TupleMatrix 2 5 where
  type HomogeneousTupleMatrix 2 5 a = ((a,a,a,a,a),(a,a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4,a_1_5),(a_2_1,a_2_2,a_2_3,a_2_4,a_2_5)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4,a_1_5],[a_2_1,a_2_2,a_2_3,a_2_4,a_2_5]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m,M.getElem @1 @5 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m,M.getElem @2 @5 m))

instance TupleMatrix 3 1 where
  type HomogeneousTupleMatrix 3 1 a = ((a),(a),(a))
  toM ((a_1_1),(a_2_1),(a_3_1)) =
    M.fromListsUnsafe
    [[a_1_1],[a_2_1],[a_3_1]]
  fromM m =
    ((M.getElem @1 @1 m),(M.getElem @2 @1 m),(M.getElem @3 @1 m))

instance TupleMatrix 3 2 where
  type HomogeneousTupleMatrix 3 2 a = ((a,a),(a,a),(a,a))
  toM ((a_1_1,a_1_2),(a_2_1,a_2_2),(a_3_1,a_3_2)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2],[a_2_1,a_2_2],[a_3_1,a_3_2]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m),(M.getElem @2 @1 m,M.getElem @2 @2 m),(M.getElem @3 @1 m,M.getElem @3 @2 m))

instance TupleMatrix 3 3 where
  type HomogeneousTupleMatrix 3 3 a = ((a,a,a),(a,a,a),(a,a,a))
  toM ((a_1_1,a_1_2,a_1_3),(a_2_1,a_2_2,a_2_3),(a_3_1,a_3_2,a_3_3)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3],[a_2_1,a_2_2,a_2_3],[a_3_1,a_3_2,a_3_3]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m))

instance TupleMatrix 3 4 where
  type HomogeneousTupleMatrix 3 4 a = ((a,a,a,a),(a,a,a,a),(a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4),(a_2_1,a_2_2,a_2_3,a_2_4),(a_3_1,a_3_2,a_3_3,a_3_4)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4],[a_2_1,a_2_2,a_2_3,a_2_4],[a_3_1,a_3_2,a_3_3,a_3_4]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m))

instance TupleMatrix 3 5 where
  type HomogeneousTupleMatrix 3 5 a = ((a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4,a_1_5),(a_2_1,a_2_2,a_2_3,a_2_4,a_2_5),(a_3_1,a_3_2,a_3_3,a_3_4,a_3_5)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4,a_1_5],[a_2_1,a_2_2,a_2_3,a_2_4,a_2_5],[a_3_1,a_3_2,a_3_3,a_3_4,a_3_5]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m,M.getElem @1 @5 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m,M.getElem @2 @5 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m,M.getElem @3 @5 m))

instance TupleMatrix 4 1 where
  type HomogeneousTupleMatrix 4 1 a = ((a),(a),(a),(a))
  toM ((a_1_1),(a_2_1),(a_3_1),(a_4_1)) =
    M.fromListsUnsafe
    [[a_1_1],[a_2_1],[a_3_1],[a_4_1]]
  fromM m =
    ((M.getElem @1 @1 m),(M.getElem @2 @1 m),(M.getElem @3 @1 m),(M.getElem @4 @1 m))

instance TupleMatrix 4 2 where
  type HomogeneousTupleMatrix 4 2 a = ((a,a),(a,a),(a,a),(a,a))
  toM ((a_1_1,a_1_2),(a_2_1,a_2_2),(a_3_1,a_3_2),(a_4_1,a_4_2)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2],[a_2_1,a_2_2],[a_3_1,a_3_2],[a_4_1,a_4_2]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m),(M.getElem @2 @1 m,M.getElem @2 @2 m),(M.getElem @3 @1 m,M.getElem @3 @2 m),(M.getElem @4 @1 m,M.getElem @4 @2 m))

instance TupleMatrix 4 3 where
  type HomogeneousTupleMatrix 4 3 a = ((a,a,a),(a,a,a),(a,a,a),(a,a,a))
  toM ((a_1_1,a_1_2,a_1_3),(a_2_1,a_2_2,a_2_3),(a_3_1,a_3_2,a_3_3),(a_4_1,a_4_2,a_4_3)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3],[a_2_1,a_2_2,a_2_3],[a_3_1,a_3_2,a_3_3],[a_4_1,a_4_2,a_4_3]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m))

instance TupleMatrix 4 4 where
  type HomogeneousTupleMatrix 4 4 a = ((a,a,a,a),(a,a,a,a),(a,a,a,a),(a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4),(a_2_1,a_2_2,a_2_3,a_2_4),(a_3_1,a_3_2,a_3_3,a_3_4),(a_4_1,a_4_2,a_4_3,a_4_4)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4],[a_2_1,a_2_2,a_2_3,a_2_4],[a_3_1,a_3_2,a_3_3,a_3_4],[a_4_1,a_4_2,a_4_3,a_4_4]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m,M.getElem @4 @4 m))

instance TupleMatrix 4 5 where
  type HomogeneousTupleMatrix 4 5 a = ((a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4,a_1_5),(a_2_1,a_2_2,a_2_3,a_2_4,a_2_5),(a_3_1,a_3_2,a_3_3,a_3_4,a_3_5),(a_4_1,a_4_2,a_4_3,a_4_4,a_4_5)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4,a_1_5],[a_2_1,a_2_2,a_2_3,a_2_4,a_2_5],[a_3_1,a_3_2,a_3_3,a_3_4,a_3_5],[a_4_1,a_4_2,a_4_3,a_4_4,a_4_5]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m,M.getElem @1 @5 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m,M.getElem @2 @5 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m,M.getElem @3 @5 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m,M.getElem @4 @4 m,M.getElem @4 @5 m))

instance TupleMatrix 5 1 where
  type HomogeneousTupleMatrix 5 1 a = ((a),(a),(a),(a),(a))
  toM ((a_1_1),(a_2_1),(a_3_1),(a_4_1),(a_5_1)) =
    M.fromListsUnsafe
    [[a_1_1],[a_2_1],[a_3_1],[a_4_1],[a_5_1]]
  fromM m =
    ((M.getElem @1 @1 m),(M.getElem @2 @1 m),(M.getElem @3 @1 m),(M.getElem @4 @1 m),(M.getElem @5 @1 m))

instance TupleMatrix 5 2 where
  type HomogeneousTupleMatrix 5 2 a = ((a,a),(a,a),(a,a),(a,a),(a,a))
  toM ((a_1_1,a_1_2),(a_2_1,a_2_2),(a_3_1,a_3_2),(a_4_1,a_4_2),(a_5_1,a_5_2)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2],[a_2_1,a_2_2],[a_3_1,a_3_2],[a_4_1,a_4_2],[a_5_1,a_5_2]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m),(M.getElem @2 @1 m,M.getElem @2 @2 m),(M.getElem @3 @1 m,M.getElem @3 @2 m),(M.getElem @4 @1 m,M.getElem @4 @2 m),(M.getElem @5 @1 m,M.getElem @5 @2 m))

instance TupleMatrix 5 3 where
  type HomogeneousTupleMatrix 5 3 a = ((a,a,a),(a,a,a),(a,a,a),(a,a,a),(a,a,a))
  toM ((a_1_1,a_1_2,a_1_3),(a_2_1,a_2_2,a_2_3),(a_3_1,a_3_2,a_3_3),(a_4_1,a_4_2,a_4_3),(a_5_1,a_5_2,a_5_3)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3],[a_2_1,a_2_2,a_2_3],[a_3_1,a_3_2,a_3_3],[a_4_1,a_4_2,a_4_3],[a_5_1,a_5_2,a_5_3]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m),(M.getElem @5 @1 m,M.getElem @5 @2 m,M.getElem @5 @3 m))

instance TupleMatrix 5 4 where
  type HomogeneousTupleMatrix 5 4 a = ((a,a,a,a),(a,a,a,a),(a,a,a,a),(a,a,a,a),(a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4),(a_2_1,a_2_2,a_2_3,a_2_4),(a_3_1,a_3_2,a_3_3,a_3_4),(a_4_1,a_4_2,a_4_3,a_4_4),(a_5_1,a_5_2,a_5_3,a_5_4)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4],[a_2_1,a_2_2,a_2_3,a_2_4],[a_3_1,a_3_2,a_3_3,a_3_4],[a_4_1,a_4_2,a_4_3,a_4_4],[a_5_1,a_5_2,a_5_3,a_5_4]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m,M.getElem @4 @4 m),(M.getElem @5 @1 m,M.getElem @5 @2 m,M.getElem @5 @3 m,M.getElem @5 @4 m))

instance TupleMatrix 5 5 where
  type HomogeneousTupleMatrix 5 5 a = ((a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a),(a,a,a,a,a))
  toM ((a_1_1,a_1_2,a_1_3,a_1_4,a_1_5),(a_2_1,a_2_2,a_2_3,a_2_4,a_2_5),(a_3_1,a_3_2,a_3_3,a_3_4,a_3_5),(a_4_1,a_4_2,a_4_3,a_4_4,a_4_5),(a_5_1,a_5_2,a_5_3,a_5_4,a_5_5)) =
    M.fromListsUnsafe
    [[a_1_1,a_1_2,a_1_3,a_1_4,a_1_5],[a_2_1,a_2_2,a_2_3,a_2_4,a_2_5],[a_3_1,a_3_2,a_3_3,a_3_4,a_3_5],[a_4_1,a_4_2,a_4_3,a_4_4,a_4_5],[a_5_1,a_5_2,a_5_3,a_5_4,a_5_5]]
  fromM m =
    ((M.getElem @1 @1 m,M.getElem @1 @2 m,M.getElem @1 @3 m,M.getElem @1 @4 m,M.getElem @1 @5 m),(M.getElem @2 @1 m,M.getElem @2 @2 m,M.getElem @2 @3 m,M.getElem @2 @4 m,M.getElem @2 @5 m),(M.getElem @3 @1 m,M.getElem @3 @2 m,M.getElem @3 @3 m,M.getElem @3 @4 m,M.getElem @3 @5 m),(M.getElem @4 @1 m,M.getElem @4 @2 m,M.getElem @4 @3 m,M.getElem @4 @4 m,M.getElem @4 @5 m),(M.getElem @5 @1 m,M.getElem @5 @2 m,M.getElem @5 @3 m,M.getElem @5 @4 m,M.getElem @5 @5 m))
