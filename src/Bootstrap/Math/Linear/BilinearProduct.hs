{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeSynonymInstances  #-}

module Bootstrap.Math.Linear.BilinearProduct
  ( BilinearProduct(..)
  ) where

import qualified Bootstrap.Math.Linear.Util as L
import           Bootstrap.Math.VectorSpace (IsBaseField, VectorSpace, type (⊗))
import qualified Bootstrap.Math.VectorSpace as VS
import           Data.Functor.Compose       (Compose (..))
import           Data.Matrix.Static         (Matrix)
import qualified Data.Matrix.Static         as M
import           GHC.TypeNats               (KnownNat)
import           Linear.Metric              (dot)
import           Linear.V                   (V)
import qualified Linear.V                   as L

-- | A class for objects that can be combined ("contracted") using an
-- inner product, including dot products of vectors, vector-matrix
-- multiplication, matrix-vector multiplication, and matrix-matrix
-- multiplication.
class (VectorSpace u, VectorSpace v) => BilinearProduct u v where
  type Contraction u v a
  -- | The dot product
  (.*) :: (Num a, IsBaseField u a, IsBaseField v a) => u a -> v a -> Contraction u v a
  infixr 7 .*

instance KnownNat n => BilinearProduct (V n) (V n) where
  type Contraction (V n) (V n) a = a
  (.*) = dot

instance (KnownNat m, KnownNat n) => BilinearProduct (Matrix m n) (V n) where
  type Contraction (Matrix m n) (V n) a = V m a
  m .* v = case M.colVector (L.toVector v) of
    Just c  -> L.fromRawVector $ M.getCol 1 (m M..* c)
    Nothing -> error "Matrix vector size mismatch"

instance (KnownNat m, KnownNat n) => BilinearProduct (V m) (Matrix m n) where
  type Contraction (V m) (Matrix m n) a = V n a
  v .* m = M.transpose m .* v

instance (KnownNat m, KnownNat n, KnownNat k) => BilinearProduct (Matrix m n) (Matrix n k) where
  type Contraction (Matrix m n) (Matrix n k) a = Matrix m k a
  (.*) = M.multStrassenMixed

instance (KnownNat n, VectorSpace v) => BilinearProduct (V n) (V n ⊗ v) where
  type Contraction (V n) (V n ⊗ v) a = v a
  u .* v = VS.sum $ VS.scale <$> u <*> getCompose v

instance (KnownNat n, VectorSpace v) => BilinearProduct (V n ⊗ v) (V n) where
  type Contraction (V n ⊗ v) (V n) a = v a
  u .* v = v .* u

instance (KnownNat m, KnownNat n, VectorSpace v) => BilinearProduct (V m) (Matrix m n ⊗ v) where
  type Contraction (V m) (Matrix m n ⊗ v) a = (V n ⊗ v) a
  u .* m = Compose $ (u .*) . Compose <$> L.toCols (getCompose m)

instance (KnownNat m, KnownNat n, VectorSpace v) => BilinearProduct (Matrix m n ⊗ v) (V n) where
  type Contraction (Matrix m n ⊗ v) (V n) a = (V m ⊗ v) a
  m .* v = v .* Compose (M.transpose (getCompose m))

instance (KnownNat m, KnownNat n, VectorSpace v) => BilinearProduct (Matrix m n) (V n ⊗ v) where
  type Contraction (Matrix m n) (V n ⊗ v) a = (V m ⊗ v) a
  m .* v = Compose $ fmap (.* v) (L.toRows m)

instance (KnownNat m, KnownNat n, VectorSpace v) => BilinearProduct (V m ⊗ v) (Matrix m n) where
  type Contraction (V m ⊗ v) (Matrix m n) a = (V n ⊗ v) a
  u .* m = Compose $ fmap (.* u) (L.toCols m)

instance (KnownNat m, KnownNat n, KnownNat k, VectorSpace v) =>
  BilinearProduct (Matrix m n) (Matrix n k ⊗ v) where
  type Contraction (Matrix m n) (Matrix n k ⊗ v) a = (Matrix m k ⊗ v) a
  m .* m' =
    Compose . L.fromCols .
    fmap (getCompose . (m .*) . Compose) .
    L.toCols . getCompose $ m'

instance (KnownNat m, KnownNat n, KnownNat k, VectorSpace v) =>
  BilinearProduct (Matrix m n ⊗ v) (Matrix n k) where
  type Contraction (Matrix m n ⊗ v) (Matrix n k) a = (Matrix m k ⊗ v) a
  m .* m' = tensorTranspose $ M.transpose m' .* (tensorTranspose m)
    where
      tensorTranspose = Compose . M.transpose . getCompose
