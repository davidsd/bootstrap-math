{-# OPTIONS_GHC -fplugin GHC.TypeLits.KnownNat.Solver #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}

module Bootstrap.Math.Linear.Instances () where

import           Data.Aeson            (FromJSON (..), FromJSONKey, ToJSON (..),
                                        ToJSONKey)
import           Data.Binary           (Binary (..))
import           Data.Distributive     (Distributive (..))
import qualified Data.Matrix           as MU
import           Data.Matrix.Static    (Matrix)
import qualified Data.Matrix.Static    as M
import           Data.Ord              (comparing)
import           Data.Vector.Instances ()
import           GHC.TypeNats          (KnownNat)
import           Linear.V              (V)

instance ToJSON a   => ToJSON (V n a)
instance FromJSON a => FromJSON (V n a)
instance ToJSON a   => ToJSONKey (V n a)
instance FromJSON a => FromJSONKey (V n a)

instance (KnownNat j, KnownNat k, ToJSON a) => ToJSON (Matrix j k a) where
  toJSON = toJSON . M.toLists

instance (KnownNat j, KnownNat k, FromJSON a) => FromJSON (Matrix j k a) where
  parseJSON = fmap M.fromListsUnsafe . parseJSON

instance (KnownNat j, KnownNat k, Binary a) => Binary (Matrix j k a) where
  put = put . M.toLists
  get = fmap M.fromListsUnsafe get

instance (KnownNat j, KnownNat k) => Distributive (Matrix j k) where
  distribute ms = M.matrix (\ij -> fmap (M.! ij) ms)

instance Ord a => Ord (MU.Matrix a) where
  compare = comparing MU.toLists

instance ToJSON a => ToJSON (MU.Matrix a) where
  toJSON = toJSON . MU.toLists

instance FromJSON a => FromJSON (MU.Matrix a) where
  parseJSON = fmap MU.fromLists . parseJSON

instance Binary a => Binary (MU.Matrix a) where
  put = put . MU.toLists
  get = fmap MU.fromLists get
