{-# OPTIONS_GHC -fplugin GHC.TypeLits.KnownNat.Solver #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}

module Bootstrap.Math.Linear.Util
  ( fromRawVector
  , toCols
  , toRows
  , fromCols
  , fromRows
  , colVector
  , rowVector
  , outerSquareWith
  , outer
  , normalize
  , head
  , tail
  , last
  , init
  , take
  , drop
  , splitAt
  , cons
  , snoc
  , (!)
  , (++)
  , diagonalMatrix
  , withSquareMatrix
  , bilinearPair
  , directSum
  , symmetrize
  ) where

import Bootstrap.Math.VectorSpace (VectorSpace (..), sum, type (⊗), (^+^))
import Control.Exception          (Exception, throw)
import Data.Foldable              qualified as Foldable
import Data.Functor.Compose       (Compose (..))
import Data.Matrix                qualified as MU
import Data.Matrix.Static         (Matrix)
import Data.Matrix.Static         qualified as M
import Data.Maybe                 (fromJust)
import Data.Proxy                 (Proxy (..))
import Data.Type.Equality         ((:~:) (..))
import Data.Vector                qualified as V
import Data.Vector.Instances      ()
import GHC.TypeNats               (KnownNat, natVal, sameNat, type (+),
                                   type (<=))
import Linear.Metric              (norm)
import Linear.V                   (Dim, V)
import Linear.V                   qualified as L
import Linear.Vector              ((^/))
import Numeric.Natural            (Natural)
import Prelude                    hiding (drop, head, init, last, splitAt, sum,
                                   tail, take, (++))

data VectorSizeError = VectorSizeError
  { expectedSize :: Natural
  , foundSize    :: Natural
  } deriving (Show, Exception)

fromRawVector :: forall j a . KnownNat j => V.Vector a -> V j a
fromRawVector v = case L.fromVector v of
  Just v' -> v'
  Nothing -> throw VectorSizeError
    { expectedSize = natVal @j Proxy
    , foundSize = fromIntegral (V.length v)
    }

colVector :: KnownNat j => V j a -> Matrix j 1 a
colVector = fromJust . M.colVector . L.toVector

rowVector :: KnownNat j => V j a -> Matrix 1 j a
rowVector = fromJust . M.rowVector . L.toVector

fromRows :: V j (V k a) -> Matrix j k a
fromRows m = M.fromListsUnsafe (Foldable.toList (fmap Foldable.toList m))

fromCols :: V k (V j a) -> Matrix j k a
fromCols = M.transpose . fromRows

toRows :: (KnownNat j, KnownNat k) => Matrix j k a -> V j (V k a)
toRows m = fromRawVector (V.generate (M.nrows m) $ \i -> fromRawVector (M.getRow (i+1) m))

toCols :: (KnownNat j, KnownNat k) => Matrix j k a -> V k (V j a)
toCols = toRows . M.transpose

infixr 5 ++
(++) :: (KnownNat n, KnownNat m) => V n a -> V m a -> V (n + m) a
u ++ v = fromRawVector (L.toVector u <> L.toVector v)

-- | First element
head :: V (j+1) a -> a
head = V.head . L.toVector

-- | Last element
last :: V (j+1) a -> a
last = V.last . L.toVector

-- | All but the first element
tail :: forall j a . KnownNat j => V (j+1) a -> V j a
tail = fromRawVector . V.tail . L.toVector

-- | Prepend an element
cons :: forall j a . KnownNat j => a -> V j a -> V (j+1) a
cons x v = fromRawVector $ V.cons x $ L.toVector v

-- | Append an element
snoc :: forall j a . KnownNat j => V j a -> a -> V (j+1) a
snoc v x = fromRawVector $ flip V.snoc x $ L.toVector v

-- | All but the last element
init :: forall j a . KnownNat j => V (j+1) a -> V j a
init = fromRawVector . V.init . L.toVector

-- | The first n elements
take :: forall n m a . KnownNat n => V (n + m) a -> V n a
take = fromRawVector . V.take n' . L.toVector
  where
    n' = fromIntegral (natVal @n Proxy)

-- | All but the first n elements
drop :: forall n m a . (KnownNat n, KnownNat m) => V (n + m) a -> V m a
drop = fromRawVector . V.drop n' . L.toVector
  where
    n' = fromIntegral (natVal @n Proxy)

-- | The first n elements, paired with the remainder
splitAt :: forall n m a . (KnownNat n, KnownNat m) => V (n + m) a -> (V n a, V m a)
splitAt v = (fromRawVector v1, fromRawVector v2)
  where
    (v1, v2) = V.splitAt n' (L.toVector v)
    n' = fromIntegral (natVal @n Proxy)

normalize :: (Dim j, Floating a) => V j a -> V j a
normalize x = x ^/ norm x

outerWith :: (KnownNat j, KnownNat k) => (a -> b -> c) -> V j a -> V k b -> M.Matrix j k c
outerWith f u v = M.matrix $ \(i,j) -> f (u ! (i-1)) (v ! (j-1))

outerSquareWith :: KnownNat n => (a -> a -> b) -> V n a -> Matrix n n b
outerSquareWith f v = outerWith f v v

outer :: (KnownNat j, KnownNat k, Num a) => V j a -> V k a -> M.Matrix j k a
outer = outerWith (*)

diagonalMatrix :: Num a => V j a -> Matrix j j a
diagonalMatrix v = M.diagonalUnsafe 0 (L.toVector v)

-- | Unsafe indexing operation (doesn't do bounds checking)
(!) :: V n a -> Int -> a
v ! i = L.toVector v V.! i
infixl 9 !

withSquareMatrix :: MU.Matrix a -> (forall n . KnownNat n => Maybe (M.Matrix n n a) -> r) -> r
withSquareMatrix mat go = M.withStatic mat $ \(mat' :: M.Matrix m n a) ->
  case sameNat (Proxy @m) (Proxy @n) of
    Just Refl -> go (Just mat')
    Nothing   -> go (Nothing :: Maybe (M.Matrix m m a))

bilinearPair
  :: (VectorSpace v, IsBaseField v a, Num a, KnownNat j)
  => V j a
  -> (Matrix j j ⊗ v) a
  -> (Matrix 1 1 ⊗ v) a
bilinearPair v (Compose m) = Compose $ M.matrix $ \_ -> sum $ do
  k <- [1 .. M.ncols m]
  l <- [1 .. M.nrows m]
  return $ scale ((v' V.! (k-1)) * (v' V.! (l-1))) (m M.! (k,l))
  where
    v' = L.toVector v

directSum
  :: (VectorSpace v, IsBaseField v a, KnownNat j, KnownNat k, 1 <= j, 1 <= k)
  => (Matrix j j ⊗ v) a
  -> (Matrix k k ⊗ v) a
  -> (Matrix (j+k) (j+k) ⊗ v) a
directSum (Compose tl) (Compose br) = Compose $
  M.joinBlocks (tl, getCompose zero, getCompose zero, br)

symmetrize
  :: (VectorSpace v, KnownNat j, IsBaseField v a, Fractional a)
  => (Matrix j j ⊗ v) a
  -> (Matrix j j ⊗ v) a
symmetrize (Compose m) = Compose $
  (\a b -> scale (1/2) (a ^+^ b)) <$> m <*> M.transpose m
