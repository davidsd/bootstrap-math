{-# LANGUAGE DeriveFunctor #-}

-- | A Stream is an infinite-length list. Here, we interpret a 'Stream
-- a' as a power series beginning with x^0 with coefficients of type
-- a. Such Streams can be elegantly manipulated to perform
-- mathematical operators on power series, as explained in
--
-- "Power Series, Power Serious" by M. Douglas Mcilroy
-- https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.38.9450
--
-- Several implementations below follow this reference.
--
-- This module is intended to be imported qualified. As an example of
-- usage, try:
--
-- >>> import qualified Bootstrap.Math.Stream as Stream
-- >>> x = Stream.x
-- >>> Stream.take 10 $ (1-x)^^(-2)
-- [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]
--
-- Stream's are the underlying data structure for the more flexible
-- 'Series' in Bootstrap.Math.Series.

module Bootstrap.Math.Stream where

import Prelude hiding (repeat, take, drop, head, tail, zipWith, iterate)
import Bootstrap.Math.VectorSpace (VectorSpace)

-- | An infinite-length list
data Stream a = a :| Stream a
  deriving (Functor)

infixr 5 :|

-- | The first element of a 'Stream'
head :: Stream a -> a
head (a:|_) = a

-- | Everything except the first element. Equivalent to 'drop 1'
tail :: Stream a -> Stream a
tail (_:|as) = as

-- | Take the first 'n' elements of a stream. If 'n' is non-positive,
-- return the empty list. Note: take always returns a finite list.
take :: Int -> Stream a -> [a]
take n (a:|as)
  | n <= 0    = []
  | otherwise = a : take (n-1) as

-- | Drop the first 'n' elements of a 'Stream'. If 'n' is
-- non-positive, return the 'Stream'.
drop :: Int -> Stream a -> Stream a
drop n s@(_:|as)
  | n <= 0    = s
  | otherwise = drop (n-1) as

-- | A Stream of repeated 'a's
repeat :: a -> Stream a
repeat a = a :| repeat a

-- | The infinite sequence of repeated applications of 'f' to 'x'.
iterate :: (a -> a) -> a -> Stream a
iterate f a = a :| iterate f (f a)

-- | Append the given List to the beginning of a Stream.
prefix :: [a] -> Stream a -> Stream a
prefix [] s = s
prefix (a:as) s = a :| prefix as s

-- | Convert a List to a Stream. If the list is finite, append an
-- infinite stream of 0's.
fromList :: Num a => [a] -> Stream a
fromList as = prefix as (repeat 0)

-- | Convert to an infinite list
toList :: Stream a -> [a]
toList (a:|as) = a : toList as

-- | A Stream representing the variable x.
x :: Num a => Stream a
x = fromList [0,1]

-- | Mathematical composition of power series.  Implementation from
-- Mcilroy.
compose :: (Eq a, Num a) => Stream a -> Stream a -> Stream a
compose (f:|fs) (0:|gs) = f :| gs * compose fs (0:|gs)
compose _ _ = error "Cannot compose with a stream whose first element is nonzero"

-- | Collapse a power series of power series into a single power
-- series
collapse :: Num a => Stream (Stream a) -> Stream a
collapse ((a:|as) :| b :| bs) = a :| collapse ((as+b) :| bs)

zipWith :: (a -> b -> c) -> Stream a -> Stream b -> Stream c
zipWith f = go
  where
    go (a:|as) (y:|ys) = f a y :| go as ys

-- | A Stream of the natural numbers 1,2,...
naturals :: Num a => Stream a
naturals = iterate (+1) 1

nonnegativeIntegers :: Num a => Stream a
nonnegativeIntegers = iterate (+1) 0

-- | Integrate 's' as a power series, with integration constant 'c'.
integrate :: Fractional a => a -> Stream a -> Stream a
integrate c s = c :| zipWith (/) s naturals

-- | The 'Num' instance interprets a stream as an infinite power series
-- beginning with x^0. Implementation from Mcilroy.
instance Num a => Num (Stream a) where
  (f:|fs) + (g:|gs) = f+g :| fs+gs
  (f:|fs) * (g:|gs) = f*g :| fmap (f*) gs + fmap (g*) fs + (0 :| fs*gs)
  negate        = fmap negate
  abs           = fmap abs
  signum        = fmap signum
  fromInteger c = fromInteger c :| repeat 0

instance VectorSpace Stream

-- | Implementation from Mcilroy.
instance Fractional a => Fractional (Stream a) where
  (f:|fs) / (g:|gs) =
    let q = f/g
    in q :| (fs - fmap (q*) gs)/(g:|gs)
  fromRational r = fromRational r :| repeat 0

-- | Multiply two streams using a general 'mul' operation for
-- combining coefficients.
mulWith :: Num c => (a -> b -> c) -> Stream a -> Stream b -> Stream c
mulWith mul = go
  where
    go (f:|fs) (g:|gs) = (f `mul` g) :| fmap (f `mul`) gs + fmap (`mul` g) fs + (0 :| go fs gs)

-- | Divide two streams using general 'div' and 'mul' operations
-- that combine two different types. This could be useful if the
-- denominator is the scalar field over which the numerator is a
-- module.
divWith :: Num a => (a -> b -> a) -> (a -> b -> a) -> Stream a -> Stream b -> Stream a
divWith div' mul s (g:|gs) = go s
  where
    go (f:|fs) =
      let q = div' f g
      in q :| go (fs - fmap (mul q) gs)

-- | Divide the a 'Stream' by another 'Stream', assuming the
-- denominator starts with 1. Note that this only requires a 'Num a'
-- instance, as opposed the 'Fractional a' required by (/).
div1 :: (Eq a, Num a) => Stream a -> Stream a -> Stream a
div1 s (1:|gs) = go s
  where
    go (f:|fs) = f :| go (fs - fmap (f*) gs)
div1 _ _ = error "div1: the denominator must start with 1"

----------- Fractional series ----------------
--
-- We provide Taylor series expansions of certain functions around
-- special points. These Series only require a Fractional instance and
-- are useful for exact arithmetic.

-- | Coefficients of the Taylor series expansion of 'exp' around 0.
exp0Expansion :: Fractional a => Stream a
exp0Expansion = go 0 1
  where
    go i e = e :| go (i+1) (e/(i+1))

-- | Coefficients of the Taylor series expansion of log (1+x)
log1pExpansion :: Fractional a => Stream a
log1pExpansion = 0 :| go 1 1
  where
    go i e = e/i :| go (i+1) (-e)

-- | Coefficients of the Taylor series expansion of (1+x)^c.
binomial1pExpansion :: Fractional a => a -> Stream a
binomial1pExpansion c = go 0 1
  where
    go i e = e :| go (i+1) ((c-i)*e/(i+1))

sin_cos_helper :: Fractional a => a -> a -> Stream a
sin_cos_helper = go 0 1
  where
    go i e f g = f*e :| go (i+1) (e/(i+1)) g (-f)

-- | Coefficients of the Taylor series expansion of 'sin' around 0.
sin0Expansion :: Fractional a => Stream a
sin0Expansion = sin_cos_helper 0 1

-- | Coefficients of the Taylor series expansion of 'cos' around 0.
cos0Expansion :: Fractional a => Stream a
cos0Expansion = sin_cos_helper 1 0

-- | Coefficients of the Taylor series expansion of 'asin' around 0.
asin0Expansion :: (Eq a, Fractional a) => Stream a
asin0Expansion =
  integrate 0 $ compose (binomial1pExpansion (-1/2)) (fromList [0,0,-1])

----------- Floating series ----------------
--
-- Taylor series expansions around generic points in general require a
-- 'Floating' instance.

-- | Coefficients of the Taylor series expansion of 'exp' around 'a'.
expExpansion :: Floating a => a -> Stream a
expExpansion a = fmap (exp a *) exp0Expansion

-- | Coefficients of the Taylor series expansion of 'log' around 'a'.
logExpansion :: Floating a => a -> Stream a
logExpansion a = log a :| go 1 (1/a)
  where
    go i e = e/i :| go (i+1) (-e/a)

-- | Coefficients of the Taylor series expansion of 'sin' around 'a'.
sinExpansion :: Floating a => a -> Stream a
sinExpansion a = sin_cos_helper (sin a) (cos a)

-- | Coefficients of the Taylor series expansion of 'cos' around 'a'.
cosExpansion :: Floating a => a -> Stream a
cosExpansion a = sin_cos_helper (cos a) (-sin a)

-- | Coefficients of the Taylor series expansion of x^c around 'x'.
binomialExpansion :: Floating a => a -> a -> Stream a
binomialExpansion c a = go 0 (a**c)
  where
    go i e = e :| go (i+1) ((c-i)*e/((i+1)*a))

-- | Coefficients of the Taylor series expansion of 'asin' around 'a'.
asinExpansion :: (Eq a, Floating a) => a -> Stream a
asinExpansion a =
  integrate (asin a) $
  compose
  (binomialExpansion (-1/2) (1-a*a))
  (fromList [0,-2*a,-1])

-- | Coefficients of the Taylor series expansion of 'acos' around 'a'.
acosExpansion :: (Eq a, Floating a) => a -> Stream a
acosExpansion a = acos a :| fmap negate rest
  where
    _ :| rest = asinExpansion a

-- | Coefficients of the Taylor series expansion of 'atan' around 'a'.
atanExpansion :: (Eq a, Floating a) => a -> Stream a
atanExpansion a =
  integrate (atan a) $
  compose
  (binomialExpansion (-1) (1+a*a))
  (fromList [0,2*a,1])

-- | Coefficients of the Taylor series expansion of 'asinh' around 'a'.
asinhExpansion :: (Eq a, Floating a) => a -> Stream a
asinhExpansion a =
  integrate (asinh a) $
  compose
  (binomialExpansion (-1/2) (1+a*a))
  (fromList [0,2*a,1])

-- | Coefficients of the Taylor series expansion of 'acosh' around 'a'.
acoshExpansion :: (Eq a, Floating a) => a -> Stream a
acoshExpansion a =
  integrate (acosh a) $
  compose
  (binomialExpansion (-1/2) (a*a-1))
  (fromList [0,2*a,1])

-- | Coefficients of the Taylor series expansion of 'atanh' around 'a'.
atanhExpansion :: (Eq a, Floating a) => a -> Stream a
atanhExpansion a =
  integrate (atanh a) $
  compose
  (binomialExpansion (-1) (1-a*a))
  (fromList [0,-2*a,-1])
