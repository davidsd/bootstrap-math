{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving     #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Bootstrap.Math.VectorSpace
  ( VectorSpace(..)
  , Tensor
  , DirectSum(..)
  , type (⊗)
  , type (⊕)
  , getDirectSum
  , (*^)
  , (^*)
  , (^/)
  , (^+^)
  , (^-^)
  , fromScalar
  , mapTensor
  ) where

import qualified Data.Foldable         as Foldable
import           Data.Functor.Compose  (Compose (..))
import           Data.Functor.Identity (Identity)
import           Data.Kind             (Constraint)
import           Data.Matrix.Static    (Matrix)
import           GHC.TypeNats          (KnownNat)
import           Linear.V              (Dim, V)
import           Prelude               hiding (sum)

class VectorSpace v where
  -- | A constraint that the base field must satisfy. For example,
  -- 'Polynomial a' requires an '(Eq a, Num a)' constraint because
  -- equality checking is needed for normalizing
  -- polynomials. Similarly, 'DampedRational' requires an '(Eq a,
  -- Fractional a)' constraint because we must convert Rationals to
  -- 'a's when performing addition.
  type IsBaseField v a :: Constraint
  type IsBaseField v a = (Num a)

  zero :: IsBaseField v a => v a
  default zero :: Num (v a) => v a
  zero = 0
  
  scale :: IsBaseField v a => a -> v a -> v a
  default scale :: (Functor v, Num a) => a -> v a -> v a
  scale a = fmap (*a)

  add :: IsBaseField v a => v a -> v a -> v a
  default add :: Num (v a) => v a -> v a -> v a
  add = (+)

  -- | 'sum' is included in the 'VectorSpace' class because some types
  -- may admit more efficient implementations than the default
  sum :: (IsBaseField v a, Foldable t, Functor t) => t (v a) -> v a
  sum = Foldable.foldl' add zero

instance Dim n => VectorSpace (V n)

instance VectorSpace Identity

instance (KnownNat j, KnownNat k) => VectorSpace (Matrix j k) where
  -- It should be possible to use the default implementation here, but
  -- there was a bug in the 'Num' instance for 'matrix-static' before
  -- 7/30/22 that made 0 give an incorrectly-sized matrix.
  zero = pure 0

-- | A composition of functors plays the role of the tensor product.
--   For example,
--
-- > (V n `Tensor` V m) a ~ Compose (V n) (V m) a ~ V n (V m a)
--
--   is equivalent to a tensor product of n-dimensional and m
--   dimensional vectors.
type Tensor = Compose
infixr 9 `Tensor`

-- | We provide a unicode synonym for `Tensor`, in case users are
-- comfortable with using Unicode.
type u ⊗ v = u `Tensor` v
infixr 9 ⊗

instance (Applicative f, VectorSpace u) => VectorSpace (f ⊗ u) where
  type IsBaseField (f ⊗ u) a = IsBaseField u a
  zero = Compose (pure zero)
  scale a = mapTensor (scale a)
  add (Compose v) (Compose v') = Compose (liftA2 add v v')

-- | A direct sum of two 'VectorSpace's
newtype DirectSum u v a = MkDirectSum (u a, v a)
  deriving newtype (Eq, Ord, Show)

-- | Convenient Unicode notation for 'DirectSum'
type u ⊕ v = u `DirectSum` v
infixr 8 ⊕

getDirectSum :: (u ⊕ v) a -> (u a, v a)
getDirectSum (MkDirectSum s) = s

instance (VectorSpace u, VectorSpace v) => VectorSpace (u ⊕ v) where
  type IsBaseField (u ⊕ v) a = (IsBaseField u a, IsBaseField v a)
  zero = MkDirectSum (zero, zero)
  scale c (MkDirectSum (f, g)) = MkDirectSum (scale c f, scale c g)
  add (MkDirectSum (f, g)) (MkDirectSum (f', g')) = MkDirectSum (add f f', add g g')

(*^) :: (VectorSpace v, IsBaseField v a, Num a, Eq a) => a -> v a -> v a
(*^) 0 _ = zero
(*^) 1 x = x
(*^) a x = scale a x
infixl 7 *^

(^*) :: (VectorSpace v, IsBaseField v a, Num a, Eq a) => v a -> a -> v a
(^*) = flip (*^)
infixl 7 ^*

(^/) :: (VectorSpace v, IsBaseField v a, Fractional a) => v a -> a -> v a
(^/) v a = scale (1/a) v
infixl 7 ^/

(^+^) :: (VectorSpace v, IsBaseField v a) => v a -> v a -> v a
(^+^) = add
infixl 6 ^+^

(^-^) :: (VectorSpace v, IsBaseField v a, Num a) => v a -> v a -> v a
(^-^) u v = add u (scale (-1) v)
infixl 6 ^-^

-- | If 'v a' is a 'Num' instance, then it admits a unit and thus a
-- canonical embedding of 'a'.
fromScalar :: (VectorSpace v, Num a, Eq a, IsBaseField v a, Num (v a)) => a -> v a
fromScalar a = a *^ 1
  
mapTensor
  :: Functor f
  => (g a -> h b)
  -> (f ⊗ g) a
  -> (f ⊗ h) b
mapTensor f = Compose . fmap f . getCompose
