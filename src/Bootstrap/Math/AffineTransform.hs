{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DeriveAnyClass  #-}
{-# LANGUAGE DeriveFunctor   #-}
{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE PolyKinds       #-}

module Bootstrap.Math.AffineTransform where

import qualified Bootstrap.Math.Linear as L
import           Data.Aeson            (FromJSON, ToJSON)
import           Data.Binary           (Binary)
import qualified Data.Matrix.Static    as M
import           GHC.Generics          (Generic)
import           GHC.TypeNats          (KnownNat)
import           Linear.Matrix         ((!*!), (*!))
import           Linear.V              (V)

-- | The transform: (\v -> affineShift + affineLinear^T v). The
-- convention affineLinear^T v is so that the rows of affineLinear are
-- the basis vectors for the image of the unit cube.
data AffineTransform n a = AffineTransform
  { affineShift  :: V n a
  , affineLinear :: V n (V n a)
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, Functor)

apply :: (KnownNat n, Num a) => AffineTransform n a -> V n a -> V n a
apply (AffineTransform s m) v = s + v *! m

inverse
  :: (KnownNat n, Fractional a, Eq a)
  => AffineTransform n a
  -> AffineTransform n a
inverse (AffineTransform shift m) = AffineTransform
  { affineShift = fmap negate (shift *! mInv)
  , affineLinear = mInv
  }
  where
    mInv = L.toRows . either error id . M.inverse . L.fromRows $ m

compose
  :: (Num a, KnownNat n)
  => AffineTransform n a
  -> AffineTransform n a
  -> AffineTransform n a
compose (AffineTransform s1 m1) (AffineTransform s2 m2) = AffineTransform
  { affineShift = s1 + s2 *! m1
  , affineLinear = m2 !*! m1
  }
