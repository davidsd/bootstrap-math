{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE DerivingVia                #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}

module Bootstrap.Math.Polynomial where

import           Bootstrap.Math.VectorSpace (VectorSpace (..), (^*))
import           Control.DeepSeq            (NFData)
import           Data.Aeson                 (FromJSON (..), ToJSON (..))
import           Data.Binary                (Binary (..))
import           Data.Coerce                (Coercible, coerce)
import           Data.Containers.ListUtils  (nubOrd)
import qualified Data.Euclidean             as Euclidean
import           Data.Maybe                 (fromMaybe)
import qualified Data.Poly                  as Poly
import           Data.Ratio
import           Data.Vector                (Vector)
import qualified Data.Vector                as V
import           Data.Vector.Binary         ()
import           Numeric.Limits             (epsilon)
import           Prelude                    hiding (quot, quotRem, (^), (^^))
import qualified Prelude

newtype Polynomial a = Polynomial { unPolynomial :: Poly.VPoly a }
  deriving newtype (Eq, Ord, Show, Num, NFData)

instance ToJSON a => ToJSON (Polynomial a) where
  toJSON = toJSON . Poly.unPoly . unPolynomial
  toEncoding = toEncoding . Poly.unPoly . unPolynomial

instance (FromJSON a, Num a, Eq a) => FromJSON (Polynomial a) where
  parseJSON = fmap (Polynomial . Poly.toPoly) . parseJSON

instance (Num a, Eq a, Binary a) => Binary (Polynomial a) where
  put = put . Poly.unPoly . unPolynomial
  get = fmap (Polynomial . Poly.toPoly) get

liftPoly :: (Poly.VPoly a -> Poly.VPoly b) -> Polynomial a -> Polynomial b
liftPoly f = Polynomial . f . unPolynomial

instance VectorSpace Polynomial where
  type IsBaseField Polynomial a = (Num a, Eq a)
  scale a = liftPoly (Poly.scale 0 a)

coefficients :: Polynomial a -> Vector a
coefficients = Poly.unPoly . unPolynomial

-- | A vector of coefficients, padded to have length >= 1.
paddedCoefficients :: Num a => Polynomial a -> Vector a
paddedCoefficients p =
  let v = coefficients p
  in case V.length v of
    0 -> V.singleton 0
    _ -> v

-- | Remove all leading coefficients less than sqrt epsilon. Used in
-- 'Bootstrap.SDP.Write'.
chopCoefficients :: RealFloat a => Polynomial a -> Polynomial a
chopCoefficients =
  fromVector .
  V.reverse . V.dropWhile (\c -> abs c <= sqrt epsilon) . V.reverse .
  coefficients

mapCoefficients :: (Eq b, Num b) => (a -> b) -> Polynomial a -> Polynomial b
mapCoefficients f = fromVector . fmap f . coefficients

fromVector :: (Eq a, Num a) => Vector a -> Polynomial a
fromVector = Polynomial . Poly.toPoly

fromList :: (Eq a, Num a) => [a] -> Polynomial a
fromList = fromVector . V.fromList

constant :: (Num a, Eq a) => a -> Polynomial a
constant = fromVector . V.singleton

constantTerm :: Num a => Polynomial a -> a
constantTerm p = case coefficients p V.!? 0 of
  Just a  -> a
  Nothing -> 0

leadingTerm :: Num a => Polynomial a -> a
leadingTerm (Polynomial p) = case Poly.leading p of
  Just (_, c) -> c
  Nothing     -> 0

x :: (Eq a, Num a) => Polynomial a
x = fromList [0, 1]

eval :: Num a => Polynomial a -> a -> a
eval (Polynomial p) y = Poly.eval p y

compose :: (Eq a, Num a) => Polynomial a -> Polynomial a -> Polynomial a
compose (Polynomial p) (Polynomial q) = Polynomial $ Poly.subst p q

derivative :: (Num a, Eq a) => Polynomial a -> Polynomial a
derivative = liftPoly Poly.deriv

shift :: (Eq a, Num a) => a -> Polynomial a -> Polynomial a
shift y = flip compose (x + constant y)

-- | Careful: the definition of Euclidean.degree for Polynomials
-- currently differs from this one.
degree :: Polynomial a -> Int
degree (Polynomial p) = case Poly.leading p of
  Just (n,_) -> fromIntegral n
  Nothing    -> 0

coercePoly :: (Eq b, Num b) => Coercible a b => Poly.VPoly a -> Poly.VPoly b
coercePoly = Poly.toPoly . coerce . Poly.unPoly

quotRem :: forall a . (Eq a, Fractional a) => Polynomial a -> Polynomial a -> (Polynomial a, Polynomial a)
quotRem (Polynomial p1) (Polynomial p2) = (Polynomial (coercePoly q), Polynomial (coercePoly r))
  where
    (q,r) = Euclidean.quotRem p1' p2'
    p1' :: Poly.VPoly (Euclidean.WrappedFractional a) = coercePoly p1
    p2' :: Poly.VPoly (Euclidean.WrappedFractional a) = coercePoly p2

quot :: (Eq a, Fractional a) => Polynomial a -> Polynomial a -> Polynomial a
quot p = fst . quotRem p

-- | Assumes a and b are trimmed and at least one non-zero returned
-- gcd is monic
gcdWithMultipliers
  :: (Eq a, Fractional a)
  => Polynomial a
  -> Polynomial a
  -> (Polynomial a, Polynomial a, Polynomial a)
gcdWithMultipliers a b
  | a == 0 = (b ^* recip (leadingTerm b), fromList [], constant (leadingTerm b))
  | b == 0 = (a ^* recip (leadingTerm a), constant (leadingTerm a), fromList [])
  | otherwise =
    case gcdWithMultipliers r b' of
      (gcd', ml, mr) ->
        if swap
          then (gcd', mr, ml + mr * q)
          else (gcd', ml + mr * q, mr)
  where
    (a', b', swap) =
      if degree a > degree b
        then (a, b, False)
        else (b, a, True)
    (q, r) = quotRem a' b'

toIntegerCoefficients :: Polynomial Rational -> Polynomial Integer
toIntegerCoefficients p =
  fromVector . fmap (\r -> (numerator r `div` gcd_) * (lcm_ `div` denominator r)) $ as
  where
    lcm_ = abs . V.foldl1 lcm $ fmap denominator as
    gcd_ = abs . V.foldl1 gcd $ fmap numerator as
    as = coefficients p

-- | Assume that the polynomial is trimmed and non-zero computes the
-- result without multiplicities
rationalRoots_ :: Polynomial Integer -> [Rational]
rationalRoots_ pol
  | V.length as < 2 = [] -- constant
  | i + 1 == V.length as = [0] -- proportional to x^n
  | otherwise = filter (\r -> eval (mapCoefficients fromInteger pol) r == 0) ((0 : tests) ++ fmap negate tests)
  where
    as = coefficients pol
    i = fromMaybe undefined $ V.findIndex (/= 0) as
    tests = nubOrd [p % q | p <- factors (abs a0), q <- factors (abs an)]
    an = V.last as
    a0 = as V.! i
    factors n = factors' [1 ..]
      where
        factors' (m:ms)
          | m * m == n = [m]
          | m * m > n = []
          | otherwise =
            let (q, r) = Prelude.quotRem n m
             in if r == 0
                  then q : m : (factors' ms)
                  else factors' ms
        factors' [] = []

-- Can make this a bit faster by folding instead of mapping (so that we divide lower and lower degree polys)
rationalRoots :: Polynomial Rational -> [(Rational, Int)]
rationalRoots pol = map (\r -> (r, go pol r)) . rationalRoots_ . toIntegerCoefficients $ pol
  where
    go :: Polynomial Rational -> Rational -> Int
    go p a =
      if eval p a == 0
        then 1 + go (p `quot` (x - (constant a))) a
        else 0
