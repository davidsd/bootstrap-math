{-# LANGUAGE TypeFamilies #-}

module Bootstrap.Math.RationalFunction
  ( RationalFunction
  , numerator
  , denominator
  , mkRationalFunction
  , fromPolynomial
  , mapNumerator
  , mapDenominator
  , constant
  , shift
  , eval
  , x
  ) where

import           Bootstrap.Math.Polynomial  (Polynomial)
import qualified Bootstrap.Math.Polynomial  as Polynomial
import           Bootstrap.Math.VectorSpace (VectorSpace(..), (*^))

data RationalFunction a = RationalFunction
  { numerator   :: Polynomial a
    -- | The denominator should always be monic. This is enforced by
    -- the smart constructor 'mkRationalFunction'.
  , denominator :: Polynomial a
  } deriving (Eq, Ord)

-- | A "smart constructor" for RationalFunction: divide two
-- 'Polynomial's and make the denominator monic
mkRationalFunction :: (Fractional a, Eq a) => Polynomial a -> Polynomial a -> RationalFunction a
mkRationalFunction num den = cancel $ RationalFunction (c *^ num) (c *^ den)
  where
    c = recip . Polynomial.leadingTerm $ den

-- | Convert a Polynomial to a RationalFunction (with denominator 1)
fromPolynomial :: (Num a, Eq a) => Polynomial a -> RationalFunction a
fromPolynomial p = RationalFunction p (Polynomial.constant 1)

-- | Map a function over the numerator of a RationalFunction. (Note
-- that the RationalFunction remains monic, so we don't need to call
-- 'mkRationalFunction' in this case.)
mapNumerator :: (Polynomial a -> Polynomial a) -> RationalFunction a -> RationalFunction a
mapNumerator f (RationalFunction num den) = RationalFunction (f num) den

-- | Map a function over the denominator of a RationalFunction.
mapDenominator
  :: (Fractional a, Eq a)
  => (Polynomial a -> Polynomial a)
  -> RationalFunction a
  -> RationalFunction a
mapDenominator f (RationalFunction num den) = mkRationalFunction num (f den)

-- | The rational function 'x'
x :: (Num a, Eq a) => RationalFunction a
x = fromPolynomial Polynomial.x

-- | A constant 'RationalFunction'
constant :: (Num a, Eq a) => a -> RationalFunction a
constant a = fromPolynomial . Polynomial.constant $ a

instance (Eq a, Fractional a) => Num (RationalFunction a) where
  (+) = addRationalFunction
  (*) = mulRationalFunction
  negate   = mapNumerator negate
  abs _    = error "abs not defined for RationalFunction"
  signum _ = error "signum not defined for RationalFunction"
  fromInteger = constant . fromInteger

instance (Eq a, Fractional a) => Fractional (RationalFunction a) where
  (/) = divRationalFunction
  fromRational = constant . fromRational

instance (Eq a, Num a, Show a) => Show (RationalFunction a) where
  show (RationalFunction num den) = "(" ++ show num ++ ")/(" ++ show den ++ ")"

instance VectorSpace RationalFunction where
  type IsBaseField RationalFunction a = (Fractional a, Eq a)
  scale a = mapNumerator (scale a)

cancel :: (Eq a, Fractional a) => RationalFunction a -> RationalFunction a
cancel (RationalFunction num den) =
  case Polynomial.gcdWithMultipliers num den of
    (_, num', den') -> RationalFunction num' den'

mulRationalFunction
  :: (Eq a, Fractional a)
  => RationalFunction a
  -> RationalFunction a
  -> RationalFunction a
mulRationalFunction (RationalFunction numa dena) (RationalFunction numb denb) =
  mkRationalFunction (numa * numb) (dena * denb)

addRationalFunction
  :: (Eq a, Fractional a)
  => RationalFunction a
  -> RationalFunction a
  -> RationalFunction a
addRationalFunction (RationalFunction numa dena) (RationalFunction numb denb) =
  mkRationalFunction (numa * denb + numb * dena) (dena * denb)

divRationalFunction
  :: (Eq a, Fractional a)
  => RationalFunction a
  -> RationalFunction a
  -> RationalFunction a
divRationalFunction (RationalFunction numa dena) (RationalFunction numb denb) =
  mkRationalFunction (numa * denb) (dena * numb)

-- Note that shifting preserves monic-ness of the denominator, so we
-- do not have to use mkRationalFunction.
shift :: (Num a, Eq a) => a -> RationalFunction a -> RationalFunction a
shift y (RationalFunction num den) =
  RationalFunction (Polynomial.shift y num) (Polynomial.shift y den)

-- | Evaluate a 'RationalFunction' at x = y
-- 
-- TODO: The ordering convention for eval is different between
-- DampedRational, Polynomial, and RationalFunction. We should make
-- this consistent at some point.
eval :: Fractional a => RationalFunction a -> a -> a
eval (RationalFunction num den) y = Polynomial.eval num y / Polynomial.eval den y
