{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}

-- | A FactorPowerExpr is a linear combination of terms of the form
--
-- \prod_i (x-c_i)^a_i * Polynomial(x),
--
-- where c_i and a_i are 'Rational's. The terms are grouped according
-- to the fractional parts of the powers k_i. For instance, if two
-- terms have the same c_i, and their a_i differ by integers, then
-- they are combined by pulling out a common factor times a
-- polynomial.
--
-- Note: We do not make an effort to factor Polynomial(x), so the
-- representation of a FactorPowerExpr is non-unique. The 'Eq' and
-- 'Ord' instances do *not* take this into account. For example, we
-- could have to FactorPowerExpr's that are mathematically equivalent
-- but not equal according to 'Eq'.
--
-- Note: This datatype is different from 'FixedPoleRational' in two
-- ways. Firstly, the positions of the factors c_i can be
-- arbitrary. Secondly, the powers a_i can be general 'Rational'
-- numbers, and are not restricted to be integers.

module Bootstrap.Math.FactorPowerExpr where

import           Bootstrap.Math.FractionalSeries (FractionalSeries)
import qualified Bootstrap.Math.FractionalSeries as FractionalSeries
import           Bootstrap.Math.FreeVect         (FreeVect, (*^))
import qualified Bootstrap.Math.FreeVect         as FreeVect
import           Bootstrap.Math.MonoidRing       (MonoidRing, Power (..))
import qualified Bootstrap.Math.MonoidRing       as MonoidRing
import           Bootstrap.Math.Polynomial       (Polynomial)
import qualified Bootstrap.Math.Polynomial       as Polynomial
import           Bootstrap.Math.Series           (pow1p, ($$))
import qualified Bootstrap.Math.Series           as Series
import           Bootstrap.Math.VectorSpace      (VectorSpace (..))
import           Data.Aeson                      (FromJSON, FromJSONKey)
import           Data.Binary                     (Binary)
import           Data.List                       (foldl1')
import qualified Data.Map.Strict                 as Map
import           Data.Proxy                      (Proxy (..))
import           GHC.Generics                    (Generic)
import           GHC.TypeLits                    (KnownSymbol, symbolVal)
import           Prelude                         hiding (sum)
import qualified Prelude

newtype FactorPowerExpr s a =
  MkFactorPowerExpr (MonoidRing (Factors s) (Polynomial a))
  deriving newtype (Eq, Ord, Binary, FromJSON)

instance (KnownSymbol s, Show a, Eq a, Num a) => Show (FactorPowerExpr s a) where
  showsPrec p (MkFactorPowerExpr e) = showsPrec p $
    MonoidRing.mapCoeffs (MonoidRing.fromPolynomial :: Polynomial a -> MonoidRing.SparseLaurent s a) e

-- | A linear factor: 'MkLinearFactor c' represents (x - c)
newtype LinearFactor s = MkLinearFactor Rational
  deriving newtype (Eq, Ord, Binary)
  deriving stock (Generic)
  deriving anyclass (FromJSON, FromJSONKey)

-- | Convert a LinearFactor to a polynomial
linearFactorToPol :: (Eq a, Fractional a) => LinearFactor s -> Polynomial a
linearFactorToPol (MkLinearFactor c) = Polynomial.x - Polynomial.constant (fromRational c)

instance KnownSymbol s => Show (LinearFactor s) where
  showsPrec p (MkLinearFactor c) = showParen (p > add_prec) $
    showString var . showString " - " . showsPrec (add_prec+1) c
    where
      add_prec = 6
      var = symbolVal @s Proxy

-- | A product of powers of linear factors: \prod_i (x - c_i)^a_i
newtype Factors s = MkFactors (FreeVect (LinearFactor s) Rational)
  deriving newtype (Eq, Ord, Binary, Semigroup, Monoid)
  deriving stock (Generic)
  deriving anyclass (FromJSON, FromJSONKey)

instance KnownSymbol s => Show (Factors s) where
  showsPrec p (MkFactors pows) =
    showParen (p > mul_prec) $ go (FreeVect.toList pows)
    where
      go []     = showString "1"
      go [x]    = showTerm x
      go (x:xs) = showTerm x . showString "*" . go xs
      showTerm (l, e) = showsPrec (exp_prec+1) l . showString "^" . showsPrec (exp_prec+1) e
      exp_prec = 8
      mul_prec = 7

-- | Convert 'Factors' into a 'Polynomial'. This is only possible if
-- all the exponents a_i are nonnegative integers. This function will
-- throw an error if it encounters an exponent that is not a
-- nonnegative integer.
factorsToPol :: (Eq a, Fractional a) => Factors s -> Polynomial a
factorsToPol (MkFactors pows) = product $ do
  (q, e) <- FreeVect.toList pows
  pure $ if
    | fractionalPart e /= 0 || e < 0 ->
      error "Cannot convert a fractional or negative power to a polynomial"
    | otherwise ->
      (linearFactorToPol q)^(floor e :: Int)

-- | The fractional part of 'x'
fractionalPart :: Rational -> Rational
fractionalPart x = x - fromIntegral (floor x :: Integer)

-- | A set of factors where the powers have been replaced with their
-- fractional parts.
fractionalPowers :: Factors s -> Factors s
fractionalPowers (MkFactors p) = MkFactors $ FreeVect.mapCoeffs fractionalPart p

-- | Take the minimum for each exponent. We assume that the fractional
-- parts of the exponents should match. If a key is absent in one of
-- them, but present in the other, then it is assumed to be 0 in the
-- one where it is absent.
--
-- We use 'Left a' to indicate that the power is present in one of the
-- sets of factors. 'Right a' indicates that the power is present in
-- both.
minPowers :: Factors s -> Factors s -> Factors s
minPowers (MkFactors f1) (MkFactors f2) =
  MkFactors $
  FreeVect.fromMap $
  fmap getMinPower $
  Map.unionWith combineMinPower
  (fmap Left (FreeVect.toMap f1))
  (fmap Left (FreeVect.toMap f2))
  where
    combineMinPower (Left x) (Left y) = Right (min x y)
    combineMinPower _ _               = error "Should not happen"

    getMinPower (Left x)  = min 0 x
    getMinPower (Right x) = x

-- | The quotient of two 'Factors'
quotFactors :: Factors s -> Factors s -> Factors s
quotFactors (MkFactors p1) (MkFactors p2) = MkFactors (p1-p2)

-- | Convert a list of (Factors s, Polynomial a) to a 'FactorPowerExpr
-- s a'. We group the terms according to their c_i and the fractional
-- parts of their a_i. Terms in the same groups are combined, yielding
-- a polynomial, times 'minPowers' of that group.
fromList
  :: forall s a . (Eq a, Fractional a)
  => [(Factors s, Polynomial a)]
  -> FactorPowerExpr s a
fromList terms =
  MkFactorPowerExpr $ MonoidRing.fromList $ fmap mergeGroup groups
  where
    groups :: [[(Factors s, Polynomial a)]]
    groups =
      Map.elems $
      Map.fromListWith (<>) [(fractionalPowers pow, [(pow, pol)]) | (pow,pol) <- terms]

    mergeGroup
      :: [(Factors s, Polynomial a)]
      -> (Factors s, Polynomial a)
    mergeGroup g = (minPow, pols)
      where
        pols = sum [factorsToPol (pow `quotFactors` minPow) * pol | (pow, pol) <- g]
        minPow = foldl1' minPowers (map fst g)

-- | Convert a 'FactorPowerExpr' to a 'List'
toList :: FactorPowerExpr s a -> [(Factors s, Polynomial a)]
toList (MkFactorPowerExpr e) = MonoidRing.toList e

-- | A product of the given 'Factors' and 'Polynomial'
singleton :: (Eq a, Fractional a) => Factors s -> Polynomial a -> FactorPowerExpr s a
singleton f p = fromList [(f,p)]

-- | The factor (x-c)^e, as a 'FactorPowerExpr'
factorPower :: (Eq a, Fractional a) => Rational -> Rational -> FactorPowerExpr s a
factorPower c e = singleton (MkFactors (e *^ FreeVect.vec (MkLinearFactor c))) 1

instance (Eq a, Fractional a) => Num (FactorPowerExpr s a) where
  MkFactorPowerExpr e1 + MkFactorPowerExpr e2 = fromList $
    MonoidRing.toList (e1+e2)
  MkFactorPowerExpr e1 * MkFactorPowerExpr e2 = fromList $
    MonoidRing.toList (e1*e2)
  fromInteger = MkFactorPowerExpr . fromInteger
  negate (MkFactorPowerExpr e) = MkFactorPowerExpr (negate e)
  abs (MkFactorPowerExpr e) = MkFactorPowerExpr (abs e)
  signum (MkFactorPowerExpr e) = MkFactorPowerExpr (signum e)

instance VectorSpace (FactorPowerExpr s) where
  type IsBaseField (FactorPowerExpr s) a = (Eq a, Fractional a)
  scale c (MkFactorPowerExpr e) = MkFactorPowerExpr $ MonoidRing.mapCoeffs (scale c) e

-- | Map a function over the Polynomials in a 'FactorPowerExpr'
mapPol
  :: (Eq b, Num b)
  => (Polynomial a -> Polynomial b)
  -> FactorPowerExpr s a
  -> FactorPowerExpr s b
mapPol f (MkFactorPowerExpr e) = MkFactorPowerExpr $
  MonoidRing.mapCoeffs f e

-- | Map a function over the coefficients of the Polynomial in a
-- 'FactorPowerExpr'
mapCoeffs :: (Eq b, Num b) => (a -> b) -> FactorPowerExpr s a -> FactorPowerExpr s b
mapCoeffs = mapPol . Polynomial.mapCoefficients

-- | Evaluate a 'FactorPowerExpr'
eval :: Floating a => a -> FactorPowerExpr s a -> a
eval x expr = Prelude.sum $ do
  (fs, pol) <- toList expr
  pure $ Polynomial.eval pol x * evalFactors x fs

-- | Evaluate a 'Factors'
evalFactors :: Floating a => a -> Factors s -> a
evalFactors x (MkFactors fs) =
  product [(x - fromRational a) ** fromRational k | (MkLinearFactor a,k) <- FreeVect.toList fs]

-- | Expand the 'FactorPowerExpr' around infinity, yielding a
-- 'FractionalSeries' in 1/x, where x is the variable in the
-- 'FactorPowerExpr'. We do not express the fact that the variable
-- changes from x to 1/x in the types -- the user should use
-- 'TypeApplications' (or type inference) to choose the correct
-- variable name for the result.
expandAroundInfinity :: (Eq a, Fractional a) => FactorPowerExpr s a -> FractionalSeries s' a
expandAroundInfinity expr = sum $ do
  (MkFactors factors, pol) <- toList expr
  pure $
    (expandPolAroundInfinity pol *) $
    FractionalSeries.singleton
    (Pow (-(Prelude.sum $ map snd (FreeVect.toList factors)))) $
    product [ pow1p (fromRational k) $$ -fromRational a *^ Series.x
            | (MkLinearFactor a,k) <- FreeVect.toList factors ]

expandPolAroundInfinity :: (Eq a, Fractional a) => Polynomial a -> FractionalSeries s a
expandPolAroundInfinity pol = FractionalSeries.singleton 0 $
  Series.dropLeadingZeros $
  foldr (\a b -> Series.constant a + b*xInv) 0 (Polynomial.coefficients pol)
  where
    xInv = 1/Series.x
