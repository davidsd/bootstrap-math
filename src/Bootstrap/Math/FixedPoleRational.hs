{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

-- | A FixedPoleRational is a rational univariate function whose
-- (possible) poles are at known locations. It is represented in the
-- form
--
-- P(x) * \product_i (x-a_i)^{n_i},
--
-- where P(x) is a polynomial, and the n_i are integers of either
-- sign. Note that the 'FixedPoleRational' need not actually possess
-- poles at any of the locations a_i if their corresponding powers n_i
-- are nonnegative.

module Bootstrap.Math.FixedPoleRational
  ( FixedPoleRational
  , polynomialFactor
  , polePowers
  , poles
  , mkFixedPoleRational
  , x
  , constant
  , mapPol
  , mapPow
  , eval
  ) where

import qualified Bootstrap.Math.MonoidRing  as MonoidRing
import           Bootstrap.Math.Polynomial  (Polynomial)
import qualified Bootstrap.Math.Polynomial  as Polynomial
import           Bootstrap.Math.VectorSpace (VectorSpace (..), fromScalar)
import           Control.DeepSeq            (NFData)
import           Data.Binary                (Binary)
import qualified Data.Foldable              as Foldable
import           Data.Proxy                 (Proxy (..))
import           Data.Reflection            (Reifies, reflect)
import qualified Data.Vector                as V
import           GHC.Generics               (Generic)
import           GHC.TypeLits               (KnownSymbol, symbolVal)
import           GHC.TypeNats               (KnownNat)
import           Linear.V                   (V)
import qualified Linear.V                   as L

-- | 's' is the name of the variable, 'p' encodes the poles of the
-- function at the type level via 'Data.Reflection.Reifies', 'n' is
-- the number of poles, and 'a' is the base field. For example:
--
-- >>> :seti -XFlexibleInstances
-- >>> :seti -XMultiParamTypeClasses
-- >>> :seti -XDataKinds
-- >>> import Data.Reflection (Reifies(..))
-- >>> import Linear.V (V)
-- >>> import Bootstrap.Math.Linear (toV)
-- >>> data MyPoles
-- >>> instance Reifies MyPoles (V 2 Rational) where { reflect _ = toV (0,1) }
-- >>> mkFixedPoleRational 1 (toV (3,4)) :: FixedPoleRational "y" MyPoles 2 Rational
-- ((1 % 1)*y^0)*y^3*(y-1 % 1)^4
--
data FixedPoleRational s p n a = MkFixedPoleRational
  { polynomialFactor :: Polynomial a -- ^ The polynomial factor P(x)
  , polePowers       :: V n Int      -- ^ The powers of each pole n_i
  } deriving (Eq, Ord, Generic, Binary, NFData)

poles :: forall s p n a. (Reifies p (V n Rational)) => FixedPoleRational s p n a -> V n Rational
poles _ = reflect @p Proxy

instance (Eq a, Num a, Show a, Reifies p (V n Rational), KnownSymbol s) => Show (FixedPoleRational s p n a) where
  showsPrec p f@(MkFixedPoleRational pol pows) =
    showParen (p > mult_prec) $
    showsPrec (mult_prec+1) (MonoidRing.fromPolynomial pol :: MonoidRing.SparseLaurent s a) .
    go (zip (Foldable.toList (poles f)) (Foldable.toList pows))
    where
      add_prec = 6
      mult_prec = 7
      exp_prec = 8

      go [] = id
      go ((a,k):ps) =
        showString "*" . showFactor a . showString "^" . showsPrec (exp_prec+1) k . go ps

      showFactor 0 = showString varName
      showFactor a = showString "(" . showString varName . showString "-" . showsPrec (add_prec+1) a . showString ")"

      varName = symbolVal @s Proxy

-- | Construct a FixedPoleRational from its polynomial factor and pole
-- powers.
mkFixedPoleRational
  :: forall s p n a . (Eq a, Fractional a, Reifies p (V n Rational), KnownNat n)
  => Polynomial a
  -> V n Int
  -> FixedPoleRational s p n a
mkFixedPoleRational pol pow
  | pol == 0  = MkFixedPoleRational 0 0
  | otherwise = go 0 (Foldable.toList poleLocs)
  where
    poleLocs = fmap fromRational $ reflect @p Proxy
    go _ [] = MkFixedPoleRational pol pow
    go i (a:as)
      | Polynomial.eval pol a == 0 =
        let
          pol' = Polynomial.quot pol (Polynomial.x - Polynomial.constant a)
          pow' = modifyV i (+1) pow
        in
          mkFixedPoleRational pol' pow'
      | otherwise = go (i+1) as

x :: (Eq a, Fractional a, Reifies p (V n Rational), KnownNat n) => FixedPoleRational s p n a
x = mkFixedPoleRational Polynomial.x 0

constant :: (Eq a, Fractional a, Reifies p (V n Rational), KnownNat n) => a -> FixedPoleRational s p n a
constant = fromScalar

-- | Apply a function to a vector at the given location. Will throw an
-- exception if the location is out of bounds.
modifyV :: KnownNat n => Int -> (b -> b) -> V n b -> V n b
modifyV i f (L.V v) = maybe undefined id $ L.fromVector $ v V.// [(i,f (v V.! i))]

instance (Eq a, Fractional a, Reifies p (V n Rational), KnownNat n) => Num (FixedPoleRational s p n a) where
  f@(MkFixedPoleRational pol1 pow1) + MkFixedPoleRational pol2 pow2 =
    mkFixedPoleRational pol pow
    where
      pow = min <$> pow1 <*> pow2
      factors = (\a -> Polynomial.x - Polynomial.constant a) . fromRational <$> poles f
      pol = pol1 * product ((^) <$> factors <*> (pow1 - pow)) +
            pol2 * product ((^) <$> factors <*> (pow2 - pow))
  MkFixedPoleRational pol1 pow1 * MkFixedPoleRational pol2 pow2 =
    mkFixedPoleRational (pol1*pol2) (pow1+pow2)
  negate (MkFixedPoleRational pol pow) = MkFixedPoleRational (negate pol) pow
  fromInteger n = MkFixedPoleRational (fromInteger n) 0
  abs = undefined
  signum = undefined

instance (KnownNat n, Reifies p (V n Rational)) => VectorSpace (FixedPoleRational s p n) where
  type IsBaseField (FixedPoleRational s p n) a = (Fractional a, Eq a)
  scale c (MkFixedPoleRational pol pow) = mkFixedPoleRational (scale c pol) pow

-- | Map a function over the Polynomial part of a 'FixedPoleRational'
mapPol
  :: (Eq b, Fractional b, Reifies p (V n Rational), KnownNat n)
  => (Polynomial a -> Polynomial b)
  -> FixedPoleRational s p n a
  -> FixedPoleRational s p n b
mapPol f (MkFixedPoleRational pol pow) = mkFixedPoleRational (f pol) pow

-- | Map a function over the pole powers of a 'FixedPoleRational'
mapPow
  :: (Eq a, Fractional a, Reifies p (V n Rational), KnownNat n)
  => (V n Int -> V n Int)
  -> FixedPoleRational s p n a
  -> FixedPoleRational s p n a
mapPow f (MkFixedPoleRational pol pow) = mkFixedPoleRational pol (f pow)

-- | Evaluate a FixedPoleRational at the given argument. TODO: The
-- order of eval is reversed compared to Polynomial.eval. Fix this?
eval
  :: (Fractional a, Reifies p (V n Rational), KnownNat n)
  => a
  -> FixedPoleRational s p n a
  -> a
eval y f@(MkFixedPoleRational pol pow) =
  Polynomial.eval pol y * product ((^^) <$> factors <*> pow)
  where
    factors = fmap (\p -> y - fromRational p) (poles f)
