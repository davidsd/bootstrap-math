{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE TypeFamilies               #-}

module Bootstrap.Math.FreeVect
  ( FreeVect
  , showsPrecTerms
  , toMap
  , fromMap
  , coeff
  , toList
  , fromList
  , singleton
  , vec
  , multiplyWith
  , evalFreeVect
  , (/.)
  , (*^)
  , evalFreeVectM
  , applyFunctional
  , mapCoeffs
  , mapBasis
  , bindTerms
  , (//)
  , pair
  , groupTermsBy
  ) where

import           Bootstrap.Math.VectorSpace (VectorSpace (..), (*^))
import qualified Bootstrap.Math.VectorSpace as VS
import           Control.DeepSeq            (NFData)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Binary                (Binary)
import qualified Data.Foldable              as Foldable
import           Data.Functor.Compose       (Compose (..))
import           Data.Map.Strict            (Map)
import qualified Data.Map.Strict            as Map

-- | The free vector space over a type b with field k
newtype FreeVect b k = MkFreeVect (Map b k)
  deriving newtype (Eq, Ord, Binary, ToJSON, FromJSON, NFData)

-- | Show a list of terms with precedence 'p'. This is a useful
-- utility for cases when a 'FreeVect' cannot be constructed, for
-- instance due to a lack of an 'Eq' instance for 'k' or an 'Ord'
-- instance for 'b'.
showsPrecTerms :: (Show b, Show k) => Int -> [(b,k)] -> ShowS
showsPrecTerms p terms = case terms of
  []  -> showString "0"
  [x] -> showParen (p > mul_prec) $ showTerm x
  xs  -> showParen (p > add_prec) $ go xs
  where
    add_prec = 6
    mul_prec = 7
    go []     = showString "0"
    go [x]    = showTerm x
    go (x:xs) = showTerm x . showString " + " . go xs
    showTerm (y,c) = showsPrec (mul_prec+1) c . showString "*" . showsPrec (mul_prec+1) y

instance (Show b, Show k) => Show (FreeVect b k) where
  showsPrec p = showsPrecTerms p . toList

toMap :: FreeVect b k -> Map b k
toMap (MkFreeVect m) = m

-- | The coefficient of 'b' in 'v'
coeff :: (Ord b, Num k) => b -> FreeVect b k -> k
coeff b v = Map.findWithDefault 0 b (toMap v)

-- | A smart constructor that removes terms with zero coefficient
fromMap :: (Num k, Eq k) => Map b k -> FreeVect b k
fromMap m = MkFreeVect (Map.filter (/= 0) m)

-- | Create a FreeVect from a list of (term, coefficient) pairs
fromList :: (Num k, Eq k, Ord b) => [(b,k)] -> FreeVect b k
fromList = fromMap . Map.fromListWith (+)

-- | A list of (term, coefficient) pairs
toList :: FreeVect b k -> [(b,k)]
toList = Map.toList . toMap

singleton :: (Num k, Eq k) => b -> k -> FreeVect b k
singleton b c = fromMap $ Map.singleton b c

-- | A basis vector corresponding to 'b'
vec :: (Num a, Eq a) => b -> FreeVect b a
vec b = singleton b 1

instance Ord b => VectorSpace (FreeVect b) where
  type IsBaseField (FreeVect b) a = (Num a, Eq a)
  zero = MkFreeVect Map.empty
  scale a = fromMap . fmap (*a) . toMap
  add u v = fromMap $ Map.unionWith (+) (toMap u) (toMap v)
  sum us = fromMap $ Map.unionsWith (+) $ fmap toMap us

-- | This Num instance is of course incomplete, but it is extremely
-- convenient to have the usual +,- notation for addition and
-- subtraction of FreeVect's.
instance (Ord b, Num k, Eq k) => Num (FreeVect b k) where
  (+) = add
  negate = scale (-1)
  (*) = error "(*) not available for FreeVect"
  abs = mapCoeffs abs
  signum = mapCoeffs signum
  fromInteger 0 = zero
  fromInteger _ = error "'fromInteger n' not available for FreeVect for nonzero n"

instance (Num a, Eq a, Ord b) => Semigroup (FreeVect b a) where
  (<>) = add

instance (Num a, Eq a, Ord b) => Monoid (FreeVect b a) where
  mempty = zero

multiplyWith
  :: (Num k, Eq k, Ord b')
  => (b1 -> b2 -> b')
  -> FreeVect b1 k
  -> FreeVect b2 k
  -> FreeVect b' k
multiplyWith f v1 v2 = fromList $ do
  (b1, k1) <- toList v1
  (b2, k2) <- toList v2
  return (f b1 b2, k1*k2)

-- | Apply a function to each basis element and multiply by the
-- corresponding coefficient, summing the results.
evalFreeVect :: (VectorSpace v, IsBaseField v k) => (b -> v k) -> FreeVect b k -> v k
evalFreeVect f v = v // \e c -> scale c (f e)

-- | An infix version of 'flip evalFreeVect'
(/.) :: (VectorSpace v, IsBaseField v a) => FreeVect b a -> (b -> v a) -> v a
(/.) = flip evalFreeVect
infixl 1 /.

evalFreeVectM
  :: (Applicative m, VectorSpace v, IsBaseField v k)
  => (b -> m (v k))
  -> FreeVect b k
  -> m (v k)
evalFreeVectM evalBasisM = getCompose . evalFreeVect (Compose . evalBasisM)

-- | Equivalent to 'runIdentity . evalFreeVect . pure'
applyFunctional :: Num k => (b -> k) -> FreeVect b k -> k
applyFunctional f = Foldable.foldl' (\acc (e,c) -> acc + c*(f e)) 0 . toList

-- | Map a function over the basis elements of a 'FreeVect'
mapBasis :: (Num k, Eq k, Ord b2) => (b1 -> b2) -> FreeVect b1 k -> FreeVect b2 k
mapBasis f = fromMap . Map.mapKeysWith (+) f . toMap

-- | Map a function over the coefficients of a 'FreeVect'
mapCoeffs :: (Num k2, Eq k2) => (k1 -> k2) -> FreeVect b k1 -> FreeVect b k2
mapCoeffs f = fromMap . fmap f . toMap

-- | Apply a function to a basis element and its coefficient, summing
-- the results.
bindTerms :: (VectorSpace v, IsBaseField v k2) => FreeVect b1 k1 -> (b1 -> k1 -> v k2) -> v k2
bindTerms v f = VS.sum . fmap (uncurry f) . toList $ v

-- | An infix version of bindTerms
(//) :: (VectorSpace v, IsBaseField v k2) => FreeVect b1 k1 -> (b1 -> k1 -> v k2) -> v k2
(//) = bindTerms
infixl 1 //

-- | A dot product, treating the basis elements as orthonormal vectors
pair :: (Ord b, Num k) => FreeVect b k -> FreeVect b k -> k
pair u v = Foldable.sum $ Map.intersectionWith (*) (toMap u) (toMap v)

groupMapBy :: (Ord b', Ord b) => (b -> b') -> Map b a -> Map b' (Map b a)
groupMapBy f m = fmap Map.fromList $ Map.fromListWith (++) $ do
  t@(b,_) <- Map.toList m
  pure (f b, [t])

-- | Group terms according to the action of 'f' on their basis
-- elements.
groupTermsBy :: (Ord b', Ord b) => (b -> b') -> FreeVect b a -> Map b' (FreeVect b a)
groupTermsBy f (MkFreeVect m) = fmap MkFreeVect $ groupMapBy f m
